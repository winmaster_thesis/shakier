package katarzyna.shakier.Helpers;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.github.javiersantos.materialstyleddialogs.enums.Style;
import katarzyna.shakier.R;


public class CustomAchievementCardView extends RelativeLayout
{
    Context context;
    View rootView;
    CardView cardViewContainer;
    TextView textViewAchievementTitle;
    TextView textViewAchievementDescr;
    ImageView imageViewAchievement;
    MaterialStyledDialog materialStyledDialog;

    public CustomAchievementCardView(Context context)
    {
        super(context);
        this.context = context;
        Init(context);
    }

    public CustomAchievementCardView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        this.context = context;
        Init(context);
    }

    private void Init(Context context)
    {
        rootView = inflate(context, R.layout.custom_achievement_cardview, this);

        cardViewContainer = rootView.findViewById(R.id.cardViewAchievement);
        textViewAchievementTitle = rootView.findViewById(R.id.textViewAchievementTitle);
        textViewAchievementDescr = rootView.findViewById(R.id.textViewAchievementDescr);
        imageViewAchievement = rootView.findViewById(R.id.imageViewIcon);
    }

    public void Builder(String title, String shortDescr, String longDescr, Drawable image, final boolean enabled)
    {
        int dialogHeaderColor;
        String achievementTitle;
        String longDescription;

        if(enabled)
        {
            dialogHeaderColor = R.color.colorAccentRev;
            achievementTitle = title;
            longDescription = longDescr;
        }
        else
        {
            cardViewContainer.setCardBackgroundColor(getResources().getColor(R.color.colorDisabled));
            imageViewAchievement.setAlpha(0.6f);
            dialogHeaderColor = R.color.colorDisabled;
            achievementTitle = title + getResources().getString(R.string.locked);
            longDescription = getResources().getString(R.string.longDescriptionDisabled);
        }

        textViewAchievementTitle.setText(title);
        textViewAchievementDescr.setText(shortDescr);
        imageViewAchievement.setImageDrawable(image);

        materialStyledDialog = new MaterialStyledDialog.Builder(context)
                .setTitle(achievementTitle)
                .setDescription(longDescription)
                .setStyle(Style.HEADER_WITH_ICON)
                .setIcon(image)
                .setCancelable(true)
                .setHeaderColor(dialogHeaderColor)
                .setPositiveText(R.string.OkButton)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.cancel();
                    }
                })
                .build();

        cardViewContainer.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                materialStyledDialog.show();
            }
        });
    }
}
