package katarzyna.shakier;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import katarzyna.shakier.Database.AppDatabase;
import katarzyna.shakier.Database.DAOs.AlgorithmsDao;
import katarzyna.shakier.Database.DAOs.SortingResultsDao;
import katarzyna.shakier.Database.Models.SortingResults;
import katarzyna.shakier.Helpers.CustomChartLegend;
import katarzyna.shakier.Helpers.Utils;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.ValueShape;
import lecho.lib.hellocharts.util.ChartUtils;
import lecho.lib.hellocharts.view.LineChartView;

public class ChartActivity extends AppCompatActivity
{
    SortingResultsDao sortingResultsDao;
    AlgorithmsDao algorithmsDao;

    private LineChartView chart;
    private LineChartData data;
    LinearLayout layoutLegend;

    ArrayList<String> availableAlgorithms;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);

        sortingResultsDao = AppDatabase.getInstance(getApplicationContext()).sortingResultsDao();
        algorithmsDao = AppDatabase.getInstance(getApplicationContext()).algorithmsDao();

        layoutLegend = findViewById(R.id.layoutLegend);
        chart = findViewById(R.id.chart);
        populateChartWithData();
    }


    private void populateChartWithData()
    {
        List<Line> lines = new ArrayList<>();
        availableAlgorithms = (ArrayList<String>) algorithmsDao.selectAllAlgorithmNames();

        for (int i = 0; i < availableAlgorithms.size(); i++)
        {
            List<PointValue> values = new ArrayList<>();
            List<SortingResults> allResultsForAlgorithm = sortingResultsDao.getAllResultsForAlgorithm(availableAlgorithms.get(i));
            Collections.sort(allResultsForAlgorithm);

            for (int j = 0; j < allResultsForAlgorithm.size(); j++)
            {
                values.add(new PointValue(allResultsForAlgorithm.get(j).getSortingSetSize(),allResultsForAlgorithm.get(j).getSortingDurationTime()));
            }

            Line line = new Line(values);
            line.setColor(getResources().getColor(Utils.returnColorForChartByAlgorithmName(availableAlgorithms.get(i))));
            line.setShape(ValueShape.CIRCLE);
            line.setCubic(false);
            line.setFilled(false);
            line.setHasLabels(false);
            line.setHasLabelsOnlyForSelected(false);
            line.setHasLines(true);
            line.setHasPoints(true);
            lines.add(line);

            CustomChartLegend customChartLegend = new CustomChartLegend(this);
            customChartLegend.Builder(availableAlgorithms.get(i));
            layoutLegend.addView(customChartLegend);
        }

        data = new LineChartData(lines);
        chart.setLineChartData(data);

        Axis sortingSetSizeAxis = new Axis();
        sortingSetSizeAxis.setName(getString(R.string.chartXAxisDescr));
        sortingSetSizeAxis.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        sortingSetSizeAxis.setMaxLabelChars(getResources().getInteger(R.integer.axisXMaxChars));
        sortingSetSizeAxis.setHasLines(true);
        sortingSetSizeAxis.setInside(false);
        data.setAxisXBottom(sortingSetSizeAxis);

        Axis sortingTimeAxis = new Axis();
        sortingTimeAxis.setName(getString(R.string.chartTimeUnit));
        sortingTimeAxis.setMaxLabelChars(getResources().getInteger(R.integer.axisYMaxChars));
        sortingTimeAxis.setHasLines(true);
        sortingTimeAxis.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        sortingTimeAxis.setInside(false);
        data.setAxisYLeft(sortingTimeAxis);
    }
}
