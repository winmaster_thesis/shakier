package katarzyna.sortPackage.Sorting;

import java.util.ArrayList;
import java.util.List;

import katarzyna.sortPackage.Helpers.CompareMethod;
import katarzyna.sortPackage.Helpers.SortingAlgorithms;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.model.SubcolumnValue;
import lecho.lib.hellocharts.util.ChartUtils;

public class BubbleSort<T extends Comparable<T>> extends BaseSort
{
    private int indexFirst;
    private int indexSecond;

    public BubbleSort(ArrayList<T> _sortArray, CompareMethod _compareMethod)
    {
        super(_sortArray,_compareMethod);
        algorithmName = SortingAlgorithms.BUBBLE_SORT;

        indexFirst=0;
        indexSecond=0;
    }

    @Override
    public boolean isSorted()
    {
        boolean isOver=true;

        for(int i=0; i<sortArray.size()-1; i++)
        {
            if(sortComparator.compare(sortArray.get(i),sortArray.get(i+1))>0)
            {
                isOver=false;
                break;
            }
        }
        return isOver;
    }

      /*
        Every sortStep:
        -compare elements and if the condition is, swap them
     */

    @Override
    public void sortStep()
    {
        if(indexFirst<sortArray.size()-1)
        {
            if (indexSecond < sortArray.size()-1)
            {
                if(sortComparator.compare(sortArray.get(indexSecond),sortArray.get(indexSecond+1))>0)
                {
                    Object tmp;
                    tmp=sortArray.get(indexSecond);
                    sortArray.set(indexSecond,sortArray.get(indexSecond+1));
                    sortArray.set(indexSecond+1,tmp);
                }
                indexSecond++;
            }
            else
            {
                indexFirst++;
                indexSecond=0;
            }
        }
    }

    @Override
    public String printSortArray()
    {
        return sortArray.toString();
    }

    @Override
    public ColumnChartData produceChartData()
    {
        if(sortArray.get(0) instanceof Integer)
        {
            List<Column> columns = new ArrayList<>();
            for (int i = 0; i < sortArray.size(); i++)
            {
                List<SubcolumnValue> value = new ArrayList<>();

                if(indexSecond == i)
                {
                    value.add(new SubcolumnValue(Float.parseFloat(sortArray.get(i).toString()), ChartUtils.COLOR_RED));
                }
                else
                {
                    value.add(new SubcolumnValue(Float.parseFloat(sortArray.get(i).toString()), ChartUtils.COLOR_BLUE));
                }

                Column column = new Column(value);
                column.setHasLabels(true);
                columns.add(column);
            }
            return new ColumnChartData(columns);
        }
       return null;
    }
}

