package katarzyna.shakier.Database.DAOs;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import katarzyna.shakier.Database.Models.SortingResults;

@Dao
public interface SortingResultsDao
{
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertSortingResult(SortingResults result);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAll(SortingResults... results);

    @Query("SELECT COUNT(*) FROM SortingResults")
    int getAmountOfResults();

    @Query("SELECT AVG(SortingDurationTime) FROM SortingResults")
    long getAverageSortingDurationTime();

    @Query("SELECT SUM(SortingDurationTime) FROM SortingResults")
    long getSumOfAllSortingDurationTime();

    @Query("SELECT MAX(SortingDurationTime) FROM SortingResults")
    int getMaxOfAllSortingTimes();

    @Query("SELECT MIN(SortingDurationTime) FROM SortingResults")
    int getMinOfAllSortingTimes();

    @Query("SELECT AVG(SortingSetSize) FROM SortingResults")
    int getAverageOfAllSortingSetSize();

    @Query("SELECT COUNT(*) FROM SortingResults WHERE AlgorithmName Like :algorithmName")
    int getAmountOfResultsForAlgorithm(String algorithmName);

    @Query("SELECT * FROM SortingResults WHERE AlgorithmName Like :algorithmName")
    List<SortingResults> getAllResultsForAlgorithm(String algorithmName);

    @Query("SELECT SortingDurationTime FROM SortingResults WHERE AlgorithmName Like :algorithmName")
    List<Long> getAllDurationsForAlgorithm(String algorithmName);

    @Query("SELECT SortingSetSize FROM SortingResults WHERE AlgorithmName Like :algorithmName")
    List<Integer> getAllSetSizesForAlgorithm(String algorithmName);

    @Query("SELECT AVG(SortingDurationTime) FROM SortingResults WHERE AlgorithmName Like :algorithmName")
    long getAverageSortingDurationForAlgorithm(String algorithmName);

    @Query("SELECT SUM(SortingDurationTime) FROM SortingResults WHERE AlgorithmName Like :algorithmName")
    long getSumOfSortingDurationTimeForAlgorithm(String algorithmName);

    @Query("SELECT MIN(SortingDurationTime) FROM SortingResults WHERE AlgorithmName Like :algorithmName")
    long getMinOfSortingDurationTimeForAlgorithm(String algorithmName);

    @Query("SELECT MAX(SortingDurationTime) FROM SortingResults WHERE AlgorithmName Like :algorithmName")
    long getMaxOfSortingDurationTimeForAlgorithm(String algorithmName);
}
