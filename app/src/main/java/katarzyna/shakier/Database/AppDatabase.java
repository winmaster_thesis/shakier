package katarzyna.shakier.Database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import katarzyna.shakier.Database.DAOs.AchievementsDao;
import katarzyna.shakier.Database.DAOs.AlgorithmsDao;
import katarzyna.shakier.Database.DAOs.SortingResultsDao;
import katarzyna.shakier.Database.DAOs.WordsDao;
import katarzyna.shakier.Database.Models.Achievements;
import katarzyna.shakier.Database.Models.Algorithms;
import katarzyna.shakier.Database.Models.SortingResults;
import katarzyna.shakier.Database.Models.Words;

@Database(entities = {Achievements.class, Algorithms.class, SortingResults.class, Words.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase
{
    private static AppDatabase INSTANCE;

    public abstract AchievementsDao achievementsDao();
    public abstract AlgorithmsDao algorithmsDao();
    public abstract SortingResultsDao sortingResultsDao();
    public abstract WordsDao wordsDao();

    public synchronized static AppDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = buildDatabase(context);
        }
        return INSTANCE;
    }

    private static AppDatabase buildDatabase(final Context context)
    {
        return Room.databaseBuilder(context,
                AppDatabase.class,
                "shakier-database").allowMainThreadQueries()
                .addCallback(new RoomDatabase.Callback() {
                    @Override
                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                        super.onCreate(db);
                        Log.d("Shakier Database", "populating with data...");
                        new PopulateDbAsync(INSTANCE).execute();
                    }
                })
                .build();
    }

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {
        private final AchievementsDao achievementsDao;
        private final AlgorithmsDao algorithmsDao;
        private final  WordsDao wordsDao;

        PopulateDbAsync(AppDatabase instance) {
            achievementsDao = instance.achievementsDao();
            algorithmsDao = instance.algorithmsDao();
            wordsDao = instance.wordsDao();
        }

        @Override
        protected Void doInBackground(Void... voids)
        {
            achievementsDao.insertAll(Achievements.populateData());
            algorithmsDao.insertAll(Algorithms.populateData());
            wordsDao.insertAll(Words.populateData());
            return null;
        }
    }
}
