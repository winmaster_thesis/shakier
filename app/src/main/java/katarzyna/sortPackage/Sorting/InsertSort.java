package katarzyna.sortPackage.Sorting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import katarzyna.sortPackage.Helpers.CompareMethod;
import katarzyna.sortPackage.Helpers.SortingAlgorithms;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.model.SubcolumnValue;
import lecho.lib.hellocharts.util.ChartUtils;

public class InsertSort<T extends Comparable<T>> extends BaseSort
{
    private ArrayList<T> destinationArray;

    public InsertSort(ArrayList<T> _sortArray, CompareMethod _compareMethod)
    {
        super(_sortArray, _compareMethod);
        algorithmName = SortingAlgorithms.INSERT_SORT;
        destinationArray= new ArrayList<>();
    }

    public boolean isSorted()
    {
        return sortArray.isEmpty();
    }

    /*
        Every sortStep:
        -add first element of sortArray to destinationArray, then delete it
        -put all elements in destinationArray in order
     */
    public void sortStep()
    {
        destinationArray.add((T) sortArray.get(0));
        sortArray.remove(0);

        Collections.sort(destinationArray,sortComparator);
    }

    public String printSortArray()
    {
        return destinationArray+ "|"+sortArray;
    }

    @Override
    public ColumnChartData produceChartData()
    {
        if((!sortArray.isEmpty() && sortArray.get(0) instanceof Integer) || (!destinationArray.isEmpty() && destinationArray.get(0) instanceof Integer))
        {
            List<Column> columns = new ArrayList<>();
            for (int i = 0; i < destinationArray.size(); i++)
            {
                List<SubcolumnValue> value = new ArrayList<>();
                value.add(new SubcolumnValue(Float.parseFloat(destinationArray.get(i).toString()), ChartUtils.COLOR_GREEN));
                Column column = new Column(value);
                column.setHasLabels(true);
                columns.add(column);
            }
            for (int i = 0; i < sortArray.size(); i++)
            {
                List<SubcolumnValue> value = new ArrayList<>();
                value.add(new SubcolumnValue(Float.parseFloat(sortArray.get(i).toString()), ChartUtils.COLOR_BLUE));
                Column column = new Column(value);
                column.setHasLabels(true);
                columns.add(column);
            }
            return new ColumnChartData(columns);
        }
        return null;
    }
}