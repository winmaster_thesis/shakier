package katarzyna.shakier;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.race604.drawable.wave.WaveDrawable;

import java.util.ArrayList;

import katarzyna.shakier.Database.Models.SortingResults;
import katarzyna.shakier.Helpers.Utils;
import katarzyna.sortPackage.Factory.Factory;
import katarzyna.sortPackage.Helpers.CompareMethod;
import katarzyna.sortPackage.Sorting.BaseSort;
import uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetPrompt;

public class AlgorithmWarsActivity extends AppCompatActivity
{
    private GestureDetectorCompat gestureDetectorCompat;
    ImageView imageViewFlingGesture;
    ImageView imageViewProgress;
    TextView textViewPercentage;
    TextView textViewTime;
    Button buttonGoToSummary;
    Drawable mWaveDrawable;
    double progressRatio;
    int algorithmsAmount;
    ConstraintLayout constraintLayoutDuels;

    //Data to sort
    String dataType;
    ArrayList generatedListToSort;
    boolean sortOrder;
    ArrayList<String> algorithmsSet;
    ArrayList<BaseSort> sorters;
    long[] sortingResults;

    //timer
    Handler handlerTimer;
    Runnable runnableTimer;
    long startTime=0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_algorithm_wars);

        getDataFromBundle(getIntent().getExtras());

        textViewPercentage = findViewById(R.id.textViewPercentage);
        textViewTime = findViewById(R.id.textViewTime);
        buttonGoToSummary = findViewById(R.id.buttonGoToSummary);
        imageViewFlingGesture = findViewById(R.id.imageViewFlingGesture);
        imageViewProgress= findViewById(R.id.imageViewProgress);
        constraintLayoutDuels = findViewById(R.id.constraintLayoutDuels);

        gestureDetectorCompat = new GestureDetectorCompat(this, new GestureListener());

        setmWaveDrawable();
        setTimerHandling();
        textViewPercentage.setText("0" + getString(R.string.percentage));

        sortingResults= new long[algorithmsAmount];
    }

    @Override
    public void onPause()
    {
        super.onPause();
        handlerTimer.removeCallbacks(runnableTimer);
        textViewTime.setText("");
    }

    private void getDataFromBundle(Bundle bundle)
    {
        dataType = bundle.getString(getString(R.string.dataType));
        generatedListToSort = bundle.getParcelableArrayList(getString(R.string.generatedListToSort));
        sortOrder = bundle.getBoolean(getString(R.string.sortOrder));
        algorithmsSet= bundle.getStringArrayList(getString(R.string.algorithmsSet));
        algorithmsAmount = algorithmsSet.size();
    }

    private void setmWaveDrawable()
    {
        mWaveDrawable = new WaveDrawable(getDrawable(R.drawable.trophy_active));
        imageViewProgress.setImageDrawable(mWaveDrawable);
        ((WaveDrawable) mWaveDrawable).setWaveAmplitude(getResources().getInteger(R.integer.waveAmplitude));
        ((WaveDrawable) mWaveDrawable).setWaveLength(getResources().getInteger(R.integer.waveLength));
        ((WaveDrawable) mWaveDrawable).setWaveSpeed(getResources().getInteger(R.integer.waveSpeed));
    }

    private void setTimerHandling()
    {
        handlerTimer = new Handler();
        runnableTimer = new Runnable()
        {
            @Override
            public void run()
            {
                textViewTime.setText(getString(R.string.algorithmWarsTimer) + Utils.getSortingDurationTime(startTime, System.currentTimeMillis()));
                handlerTimer.postDelayed(this, getResources().getInteger(R.integer.timerHandlerPostDelayed));
            }
        };
    }

    public void goToSummaryClicked(View view)
    {
        startActivity(new Intent(AlgorithmWarsActivity.this, AlgorithmWarsSummary.class)
        .putExtra(getString(R.string.resultsList) , sortingResults)
        .putExtra(getString(R.string.algorithmsSet), algorithmsSet)
        .putExtra(getString(R.string.listSize), generatedListToSort.size()));
    }

    public void onFlingGestureIconClicked(View view)
    {
        new MaterialTapTargetPrompt.Builder(AlgorithmWarsActivity.this)
                .setTarget(findViewById(R.id.imageViewFlingGesture))
                .setPrimaryText(getString(R.string.targetPromptTitle))
                .setSecondaryText(getString(R.string.targetPromtDescr))
                .setPromptStateChangeListener(new MaterialTapTargetPrompt.PromptStateChangeListener()
                {
                    @Override
                    public void onPromptStateChanged(MaterialTapTargetPrompt prompt, int state){}
                })
                .show();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        this.gestureDetectorCompat.onTouchEvent(event);
        return super.onTouchEvent(event);
    }


    private class GestureListener extends GestureDetector.SimpleOnGestureListener
    {
        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY)
        {
            if(imageViewFlingGesture.isEnabled())
            {
                imageViewFlingGesture.setEnabled(false);
                new AlgorithmWars().execute();
            }
            return true;
        }
    }

    private class AlgorithmWars extends AsyncTask<Void, Integer,Void>
    {
        Factory factory;

        @Override
        protected void onPreExecute()
        {
            sorters = new ArrayList<>();
            factory = new Factory(generatedListToSort, sortOrder?  CompareMethod.DESCENDING : CompareMethod.ASCENDING );

            for(String algorithmName : algorithmsSet)
            {
                sorters.add(factory.produceSorter(algorithmName));
            }

            progressRatio = 0.1;
            int percentage = (int)(progressRatio * getResources().getInteger(R.integer.percentageMultiplier));
            textViewPercentage.setText(percentage + getString(R.string.percentage));
            mWaveDrawable.setLevel((int)(progressRatio * getResources().getInteger(R.integer.waveLevelMultiplier)));

            startTime = System.currentTimeMillis();
            handlerTimer.postDelayed(runnableTimer, 0);
        }

        @Override
        protected Void doInBackground(Void... voids)
        {
            for(int i = 0; i<sorters.size(); i++)
            {
                long start = System.currentTimeMillis();
                sorters.get(i).executeSorting();
                long end = System.currentTimeMillis();
                int timeDiff = (int)(end-start);
                try
                {
                    Thread.sleep(getResources().getInteger(R.integer.resultDelay));
                    publishProgress(i, timeDiff);
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            textViewPercentage.setText(getString(R.string.fullPercentage));
            buttonGoToSummary.setVisibility(View.VISIBLE);
            handlerTimer.removeCallbacks(runnableTimer);
        }

        @Override
        protected void onProgressUpdate(Integer... values)
        {
            sortingResults[values[0]] = Long.valueOf(values[1]);
            refreshUI(values[0]);
        }

        private void refreshUI(int progress)
        {
            progressRatio = (double) (progress+1) / algorithmsAmount;
            int percentage = (int)(progressRatio * getResources().getInteger(R.integer.percentageMultiplier));
            textViewPercentage.setText(percentage+ getString(R.string.percentage));
            mWaveDrawable.setLevel((int)(progressRatio * getResources().getInteger(R.integer.waveLevelMultiplier)));
        }
    }
}
