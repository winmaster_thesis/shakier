package katarzyna.sortPackage.Sorting;

import java.util.ArrayList;
import java.util.List;

import katarzyna.sortPackage.Helpers.CompareMethod;
import katarzyna.sortPackage.Helpers.SortingAlgorithms;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.model.SubcolumnValue;
import lecho.lib.hellocharts.util.ChartUtils;

public class GnomeSort<T extends Comparable<T>> extends BaseSort
{
    private int indexFirst;
    private int indexSecond;

    public GnomeSort(ArrayList<T> _sortArray, CompareMethod _compareMethod)
    {
        super(_sortArray, _compareMethod);
        algorithmName = SortingAlgorithms.GNOME_SORT;
        indexFirst=1;
        indexSecond=2;
    }

    public boolean isSorted()
    {
        boolean isOver=true;

        if(indexFirst==sortArray.size())
        {
            for(int i=0; i<sortArray.size()-1; i++)
            {
                if(sortComparator.compare(sortArray.get(i),sortArray.get(i+1))>0)
                {
                    isOver=false;
                    break;
                }
            }
        }
        else
            isOver=false;
        return isOver;
    }

    /*
      Every sortStep:
      -The algorithm always finds the first place where two adjacent elements are in the wrong order, and swaps them
    */
    public void sortStep()
    {
        if(indexFirst<sortArray.size())
        {
            if ( sortComparator.compare(sortArray.get(indexFirst-1),sortArray.get(indexFirst))<=0)
            {
                indexFirst = indexSecond;
                indexSecond++;
            }
            else
            {
                T tmp= (T) sortArray.get(indexFirst-1);
                sortArray.set(indexFirst-1, sortArray.get(indexFirst));
                sortArray.set(indexFirst,tmp);
                indexFirst--;
                if (indexFirst == 0)
                {
                    indexFirst = 1;
                    indexSecond= 2;
                }
            }
        }
    }

    public String printSortArray()
    {
        return sortArray.toString();
    }

    @Override
    public ColumnChartData produceChartData()
    {
        if(sortArray.get(0) instanceof Integer)
        {
            List<Column> columns = new ArrayList<>();
            for (int i = 0; i < sortArray.size(); i++)
            {
                List<SubcolumnValue> value = new ArrayList<>();

                if(indexFirst == i)
                {
                    value.add(new SubcolumnValue(Float.parseFloat(sortArray.get(i).toString()), ChartUtils.COLOR_RED));
                }
                else
                {
                    if(indexSecond == i)
                    {
                        value.add(new SubcolumnValue(Float.parseFloat(sortArray.get(i).toString()), ChartUtils.COLOR_GREEN));
                    }
                    else
                    {
                        value.add(new SubcolumnValue(Float.parseFloat(sortArray.get(i).toString()), ChartUtils.COLOR_BLUE));
                    }
                }

                Column column = new Column(value);
                column.setHasLabels(true);
                columns.add(column);
            }
            return new ColumnChartData(columns);
        }
       return null;
    }
}
