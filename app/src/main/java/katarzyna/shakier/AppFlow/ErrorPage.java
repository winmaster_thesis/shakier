package katarzyna.shakier.AppFlow;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.jetradar.desertplaceholder.DesertPlaceholder;

import katarzyna.shakier.Helpers.Utils;
import katarzyna.shakier.R;

public class ErrorPage extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error_page);

        Utils.incrementSharedIntSetting(getApplicationContext(), getString(R.string.BUGS_FOUND));

        DesertPlaceholder desertPlaceholder = findViewById(R.id.placeholder);
        desertPlaceholder.setOnButtonClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage( getBaseContext().getPackageName() );
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(i);
            }
        });
    }
}
