package katarzyna.shakier;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;

import katarzyna.sortPackage.Factory.Factory;
import katarzyna.sortPackage.Helpers.CompareMethod;
import katarzyna.sortPackage.Helpers.SortingAlgorithms;
import katarzyna.sortPackage.Sorting.BaseSort;

import static katarzyna.shakier.SortingTestHelper.populateIntData;
import static katarzyna.shakier.SortingTestHelper.populateStringData;
import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class SortPackageUnitTesting
{
    private ArrayList<Integer> integerListToSort;
    private ArrayList<String> stringListToSort;
    private ArrayList<Integer> intListForBogo;
    private ArrayList<String> stringListForBogo;
    private Factory factory;

    public SortPackageUnitTesting()
    {
        integerListToSort = populateIntData(1000);
        stringListToSort = populateStringData(1000);
        intListForBogo = populateIntData(10);
        stringListForBogo = populateStringData(10);
    }

    //Bubble Sort

    @Test
    public void test_bubbleSort_integer_asc()
    {
        factory = new Factory(integerListToSort, CompareMethod.ASCENDING);
        BaseSort sorter = factory.produceSorter(SortingAlgorithms.BUBBLE_SORT);
        sorter.executeSorting();
        ArrayList test = new ArrayList();
        test.addAll(integerListToSort);
        Collections.sort(test);
        assertEquals(sorter.getSortArray(), test);
    }

    @Test
    public void test_bubbleSort_integer_dsc()
    {
        factory = new Factory(integerListToSort, CompareMethod.DESCENDING);
        BaseSort sorter = factory.produceSorter(SortingAlgorithms.BUBBLE_SORT);
        sorter.executeSorting();
        ArrayList test = new ArrayList();
        test.addAll(integerListToSort);
        Collections.sort(test);
        Collections.reverse(test);
        assertEquals(sorter.getSortArray(), test);
    }

    @Test
    public void test_bubbleSort_string_asc()
    {
        factory = new Factory(stringListToSort, CompareMethod.ASCENDING);
        BaseSort sorter = factory.produceSorter(SortingAlgorithms.BUBBLE_SORT);
        sorter.executeSorting();
        ArrayList test = new ArrayList();
        test.addAll(stringListToSort);
        Collections.sort(test);
        assertEquals(sorter.getSortArray(), test);
    }

    @Test
    public void test_bubbleSort_string_dsc()
    {
        factory = new Factory(stringListToSort, CompareMethod.DESCENDING);
        BaseSort sorter = factory.produceSorter(SortingAlgorithms.BUBBLE_SORT);
        sorter.executeSorting();
        ArrayList test = new ArrayList();
        test.addAll(stringListToSort);
        Collections.sort(test);
        Collections.reverse(test);
        assertEquals(sorter.getSortArray(), test);
    }

    //Insert Sort

    @Test
    public void test_insertSort_integer_asc()
    {
        factory = new Factory(integerListToSort, CompareMethod.ASCENDING);
        BaseSort sorter = factory.produceSorter(SortingAlgorithms.INSERT_SORT);
        sorter.executeSorting();
        assertEquals(sorter.getSortArray().size(),0);
    }

    @Test
    public void test_insertSort_integer_dsc()
    {
        factory = new Factory(integerListToSort, CompareMethod.DESCENDING);
        BaseSort sorter = factory.produceSorter(SortingAlgorithms.INSERT_SORT);
        sorter.executeSorting();
        assertEquals(sorter.getSortArray().size(),0);
    }

    @Test
    public void test_insertSort_string_asc()
    {
        factory = new Factory(stringListToSort, CompareMethod.ASCENDING);
        BaseSort sorter = factory.produceSorter(SortingAlgorithms.INSERT_SORT);
        sorter.executeSorting();
        assertEquals(sorter.getSortArray().size(),0);
    }

    @Test
    public void test_insertSort_string_dsc()
    {
        factory = new Factory(stringListToSort, CompareMethod.DESCENDING);
        BaseSort sorter = factory.produceSorter(SortingAlgorithms.INSERT_SORT);
        sorter.executeSorting();
        assertEquals(sorter.getSortArray().size(),0);
    }

    //SelectSort

    @Test
    public void test_selectSort_integer_asc()
    {
        factory = new Factory(integerListToSort, CompareMethod.ASCENDING);
        BaseSort sorter = factory.produceSorter(SortingAlgorithms.SELECT_SORT);
        sorter.executeSorting();
        assertEquals(sorter.getSortArray().size(),0);
    }

    @Test
    public void test_selectSort_integer_dsc()
    {
        factory = new Factory(integerListToSort, CompareMethod.DESCENDING);
        BaseSort sorter = factory.produceSorter(SortingAlgorithms.SELECT_SORT);
        sorter.executeSorting();
        assertEquals(sorter.getSortArray().size(),0);
    }

    @Test
    public void test_selectSort_string_asc()
    {
        factory = new Factory(stringListToSort, CompareMethod.ASCENDING);
        BaseSort sorter = factory.produceSorter(SortingAlgorithms.SELECT_SORT);
        sorter.executeSorting();
        assertEquals(sorter.getSortArray().size(),0);
    }

    @Test
    public void test_selectSort_string_dsc()
    {
        factory = new Factory(stringListToSort, CompareMethod.DESCENDING);
        BaseSort sorter = factory.produceSorter(SortingAlgorithms.SELECT_SORT);
        sorter.executeSorting();
        assertEquals(sorter.getSortArray().size(),0);
    }

    //BogoSort

    @Test
    public void test_bogoSort_integer_asc()
    {
        factory = new Factory(intListForBogo, CompareMethod.ASCENDING);
        BaseSort sorter = factory.produceSorter(SortingAlgorithms.BOGO_SORT);
        sorter.executeSorting();
        ArrayList test = new ArrayList();
        test.addAll(intListForBogo);
        Collections.sort(test);
        assertEquals(sorter.getSortArray(), test);
    }

    @Test
    public void test_bogoSort_integer_dsc()
    {
        factory = new Factory(intListForBogo, CompareMethod.DESCENDING);
        BaseSort sorter = factory.produceSorter(SortingAlgorithms.BOGO_SORT);
        sorter.executeSorting();
        ArrayList test = new ArrayList();
        test.addAll(intListForBogo);
        Collections.sort(test);
        Collections.reverse(test);
        assertEquals(sorter.getSortArray(), test);
    }

    @Test
    public void test_bogoSort_string_asc()
    {
        factory = new Factory(stringListForBogo, CompareMethod.ASCENDING);
        BaseSort sorter = factory.produceSorter(SortingAlgorithms.BOGO_SORT);
        sorter.executeSorting();
        ArrayList test = new ArrayList();
        test.addAll(stringListForBogo);
        Collections.sort(test);
        assertEquals(sorter.getSortArray(), test);
    }

    @Test
    public void test_bogoSort_string_dsc()
    {
        factory = new Factory(stringListForBogo, CompareMethod.DESCENDING);
        BaseSort sorter = factory.produceSorter(SortingAlgorithms.BOGO_SORT);
        sorter.executeSorting();
        ArrayList test = new ArrayList();
        test.addAll(stringListForBogo);
        Collections.sort(test);
        Collections.reverse(test);
        assertEquals(sorter.getSortArray(), test);
    }

    //GnomeSort

    @Test
    public void test_gnomeSort_integer_asc()
    {
        factory = new Factory(integerListToSort, CompareMethod.ASCENDING);
        BaseSort sorter = factory.produceSorter(SortingAlgorithms.GNOME_SORT);
        sorter.executeSorting();
        ArrayList test = new ArrayList();
        test.addAll(integerListToSort);
        Collections.sort(test);
        assertEquals(sorter.getSortArray(), test);
    }

    @Test
    public void test_gnomeSort_integer_dsc()
    {
        factory = new Factory(integerListToSort, CompareMethod.DESCENDING);
        BaseSort sorter = factory.produceSorter(SortingAlgorithms.GNOME_SORT);
        sorter.executeSorting();
        ArrayList test = new ArrayList();
        test.addAll(integerListToSort);
        Collections.sort(test);
        Collections.reverse(test);
        assertEquals(sorter.getSortArray(), test);
    }

    @Test
    public void test_gnomeSort_string_asc()
    {
        factory = new Factory(stringListToSort, CompareMethod.ASCENDING);
        BaseSort sorter = factory.produceSorter(SortingAlgorithms.GNOME_SORT);
        sorter.executeSorting();
        ArrayList test = new ArrayList();
        test.addAll(stringListToSort);
        Collections.sort(test);
        assertEquals(sorter.getSortArray(), test);
    }

    @Test
    public void test_gnomeSort_string_dsc()
    {
        factory = new Factory(stringListToSort, CompareMethod.DESCENDING);
        BaseSort sorter = factory.produceSorter(SortingAlgorithms.GNOME_SORT);
        sorter.executeSorting();
        ArrayList test = new ArrayList();
        test.addAll(stringListToSort);
        Collections.sort(test);
        Collections.reverse(test);
        assertEquals(sorter.getSortArray(), test);
    }
}