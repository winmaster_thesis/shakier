package katarzyna.shakier;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.List;
import java.util.concurrent.ExecutionException;

import katarzyna.shakier.Database.AppDatabase;
import katarzyna.shakier.Database.DAOs.AchievementsDao;
import katarzyna.shakier.Database.Models.Achievements;
import katarzyna.shakier.Helpers.CustomAchievementCardView;
import katarzyna.shakier.Helpers.Utils;

public class AchievementsActivity extends AppCompatActivity
{
    LinearLayout linearLayout;
    List<Achievements> achievements;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_achievements);
        linearLayout = findViewById(R.id.layout);

        try
        {
            achievements = new LoadAchievements().execute().get();

            if (achievements.size()!=0)
            {
                arrangeAchievementsToCardView();
            }
            else
            {
                Toast.makeText(getApplicationContext(), getString(R.string.achivementsLoading), Toast.LENGTH_SHORT).show();
            }
        }
        catch (InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }
    }

    private void arrangeAchievementsToCardView()
    {
        for(Achievements achievement: achievements)
        {
            CustomAchievementCardView cardView = new CustomAchievementCardView(this);
            int icoId = getApplicationContext().getResources().getIdentifier(achievement.getAchievementIcoName(), "drawable", getApplicationContext().getPackageName());

            cardView.Builder(
                    achievement.getAchievementTitle(),
                    achievement.getAchievementShortInfo(),
                    achievement.getAchievementLongInfo(),
                    getDrawable(icoId),
                    checkIfAchievementIsEnabled(achievement.getNumberOfIterations(), achievement.getAchievementConnectedSharedPrefName()));
            linearLayout.addView(cardView);
            linearLayout.refreshDrawableState();
        }
    }

    private boolean checkIfAchievementIsEnabled(int iterations, String sharedPrefName)
    {
        if(iterations!=0)
        {
            int amountOfIterations = Utils.readSharedIntSetting(getApplicationContext(), sharedPrefName, 0);
            return amountOfIterations >= iterations;
        }
        else
        {
           return Utils.readSharedBoolSetting(getApplicationContext(), sharedPrefName, false);
        }
    }

    private class LoadAchievements extends AsyncTask<Void, Void, List<Achievements>> {
        @Override
        protected List<Achievements> doInBackground(Void... voids) {
            AchievementsDao achievementsDao= AppDatabase.getInstance(getApplicationContext()).achievementsDao();
            return  achievementsDao.selectAll();
        }
    }
}
