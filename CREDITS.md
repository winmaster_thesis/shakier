# Iconsets and graphics

## Iconfinder
### Circle icons set
[www.iconfinder.com/iconsets/circle-icons-1](https://www.iconfinder.com/iconsets/circle-icons-1)
> GPL License
> https://www.gnu.org/copyleft/gpl.html
### competition_prize_reward_success_trophy_win_winning_icon
[www.iconfinder.com/icons/514944/competition_prize_reward_success_trophy_win_winning_icon](https://www.iconfinder.com/icons/514944/competition_prize_reward_success_trophy_win_winning_icon)
> Free for commercial use (Include link to authors website)
> https://www.iconfinder.com/pareshdhake
### down_finger_gesture_hand_interactive_scroll_swipe_icon
[www.iconfinder.com/icons/446288/down_finger_gesture_hand_interactive_scroll_swipe_icon](https://www.iconfinder.com/icons/446288/down_finger_gesture_hand_interactive_scroll_swipe_icon)
>LICENSE
>Free for commercial use

## Flaticon
[www.flaticon.com/free-icon/shake_96986#term=shake%20phone&page=1&position=1](https://www.flaticon.com/free-icon/shake_96986#term=shake%20phone&page=1&position=1)
<div>Icons made by <a href="https://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>

# Libaries

### DesertPlaceholder
[github.com/JetradarMobile/desertplaceholder](https://github.com/JetradarMobile/desertplaceholder)

> Copyright 2016 JetRadar
> 
> Licensed under the Apache License, Version 2.0 (the "License");
> you may not use this file except in compliance with the License.
> You may obtain a copy of the License at
> 
>    http://www.apache.org/licenses/LICENSE-2.0
> 
> Unless required by applicable law or agreed to in writing, software
> distributed under the License is distributed on an "AS IS" BASIS,
> WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
> See the License for the specific language governing permissions and
> limitations under the License.

### CustomActivityOnCrash
[github.com/Ereza/CustomActivityOnCrash](https://github.com/Ereza/CustomActivityOnCrash)
> Custom Activity on Crash library
> Copyright (c) 2014-2017 Eduard Ereza, http://www.eduardereza.com/
> 
> This product is licensed under the terms of the Apache Software License 2.0. See the LICENSE file for the full license text.

### Konfetti
[github.com/DanielMartinus/Konfetti](https://github.com/DanielMartinus/Konfetti)
> ISC License
> 
> Copyright (c) 2017 Dion Segijn
> 
> Permission to use, copy, modify, and/or distribute this software for any
> purpose with or without fee is hereby granted, provided that the above
> copyright notice and this permission notice appear in all copies.
> 
> THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
> WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
> MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
> ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
> WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
> ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
> OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

### AppIntro
[github.com/PaoloRotolo/AppIntro](https://github.com/PaoloRotolo/AppIntro)
> Licensed under the Apache License, Version 2.0 (the "License");

### MaterialStyledDialogs
[github.com/javiersantos/MaterialStyledDialogs](https://github.com/javiersantos/MaterialStyledDialogs)
> Copyright 2016-2018 Javier Santos
> 
> Licensed under the Apache License, Version 2.0 (the "License");
> you may not use this file except in compliance with the License.
> You may obtain a copy of the License at
> 
>    http://www.apache.org/licenses/LICENSE-2.0
> 
> Unless required by applicable law or agreed to in writing, software
> distributed under the License is distributed on an "AS IS" BASIS,
> WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
> See the License for the specific language governing permissions and
> limitations under the License.
> 
> MaterialStyledDialogs includes code from material-dialogs, which is 
> licensed under the MIT license. You may obtain a copy at
> 
>    https://github.com/afollestad/material-dialogs/blob/master/LICENSE.txt

### HelloCharts
[github.com/lecho/hellocharts-android](https://github.com/lecho/hellocharts-android)
> HelloCharts	
> Copyright 2014 Leszek Wach
> 
> Licensed under the Apache License, Version 2.0 (the "License");
> you may not use this file except in compliance with the License.
> You may obtain a copy of the License at
> 
>    http://www.apache.org/licenses/LICENSE-2.0
> 
> Unless required by applicable law or agreed to in writing, software
> distributed under the License is distributed on an "AS IS" BASIS,
> WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
> See the License for the specific language governing permissions and
> limitations under the License.

### Rolling TextView
[github.com/YvesCheung/RollingText](https://github.com/YvesCheung/RollingText)
> Copyright 2018 Yves Cheung
> 
> Licensed under the Apache License, Version 2.0 (the "License");
> you may not use this file except in compliance with the License.
> You may obtain a copy of the License at
> 
>    	http://www.apache.org/licenses/LICENSE-2.0
> 
> Unless required by applicable law or agreed to in writing, software
> distributed under the License is distributed on an "AS IS" BASIS,
> WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
> See the License for the specific language governing permissions and
> limitations under the License.

### MaterialTapTargetPrompt
[github.com/sjwall/MaterialTapTargetPrompt](https://github.com/sjwall/MaterialTapTargetPrompt)
> Copyright (C) 2016-2018 Samuel Wall
> 
> Licensed under the Apache License, Version 2.0 (the "License");
> you may not use this file except in compliance with the License.
> You may obtain a copy of the License at
> 
> http://www.apache.org/licenses/LICENSE-2.0
> 
> Unless required by applicable law or agreed to in writing, software
> distributed under the License is distributed on an "AS IS" BASIS,
> WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
> See the License for the specific language governing permissions and
> limitations under the License.

### WaveLoading
[github.com/race604/WaveLoading](https://github.com/race604/WaveLoading)
> MIT License

### Bubble Picker
[github.com/igalata/Bubble-Picker](https://github.com/igalata/Bubble-Picker)
> MIT License
> 
> Copyright (c) 2017 Irina Galata
> 
> Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
> 
> The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
> 
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

