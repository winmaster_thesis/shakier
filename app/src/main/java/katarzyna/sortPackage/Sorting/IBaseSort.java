package katarzyna.sortPackage.Sorting;

import lecho.lib.hellocharts.model.ColumnChartData;

public interface IBaseSort
{
    boolean isSorted();
    void sortStep();
    String printSortArray();
    ColumnChartData produceChartData();
}
