package katarzyna.sortPackage.Helpers;

public enum CompareMethod
{
    DESCENDING,
    ASCENDING
}
