package katarzyna.shakier;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.concurrent.ExecutionException;

import katarzyna.shakier.Database.AppDatabase;
import katarzyna.shakier.Database.DAOs.AlgorithmsDao;
import katarzyna.shakier.Database.Models.Algorithms;

public class AlgorithmsInfoActivity extends AppCompatActivity
{
    TextView textViewTitle;
    ImageView imageViewLogo;
    TextView textViewAlgorithmStability;
    TextView textViewAlgorithmComplexity;
    TextView textViewAlgorithmStructure;
    TextView textViewAlgorithmDescrTitle;
    TextView textViewAlgorithmDescription;

    public String algorithmName;
    public Algorithms algorithm;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_algorithms_info);

        textViewTitle = findViewById(R.id.textViewTitle);
        imageViewLogo = findViewById(R.id.imageViewLogo);
        textViewAlgorithmStability = findViewById(R.id.textViewAlgorithmStability);
        textViewAlgorithmComplexity = findViewById(R.id.textViewAlgorithmComplexity);
        textViewAlgorithmStructure = findViewById(R.id.textViewAlgorithmStructure);
        textViewAlgorithmDescrTitle = findViewById(R.id.textViewAlgorithmDescrTitle);
        textViewAlgorithmDescription = findViewById(R.id.textViewAlgorithmDescription);

        Intent intent = getIntent();
        algorithmName = intent.getStringExtra(getString(R.string.algorithmName));

        try
        {
            algorithm = new LoadAlgorithm().execute().get();
        }
        catch (InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        textViewTitle.setText(algorithm.getAlgorithmName());
        int icoId = getApplicationContext().getResources().getIdentifier(algorithm.getIconName(), "drawable", getApplicationContext().getPackageName());
        imageViewLogo.setImageResource(icoId);
        textViewAlgorithmStability.setText(algorithm.getAlgorithmStability());
        textViewAlgorithmComplexity.setText(Html.fromHtml(algorithm.getAlgorithmComplexity()));
        textViewAlgorithmStructure.setText(algorithm.getAlgorithmStructure());
        textViewAlgorithmDescrTitle.setText(algorithm.getAlgorithmName());
        textViewAlgorithmDescription.setText(Html.fromHtml(algorithm.getAlgorithmInformations()));
        textViewAlgorithmDescription.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public void goNext(View view)
    {
        startActivity(new Intent(
                AlgorithmsInfoActivity.this,
                DataGenerationActivity.class)
                .putExtra(getString(R.string.algorithmName), algorithmName));
    }

    private class LoadAlgorithm extends AsyncTask<Void, Void, Algorithms>
    {
        @Override
        protected Algorithms doInBackground(Void... voids)
        {
            AlgorithmsDao algorithmsDao = AppDatabase.getInstance(getApplicationContext()).algorithmsDao();
            return algorithmsDao.findByName(algorithmName);
        }
    }
}
