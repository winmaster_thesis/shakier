package katarzyna.shakier;

import android.app.ActivityManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import cat.ereza.customactivityoncrash.config.CaocConfig;
import katarzyna.shakier.AppFlow.ErrorPage;
import katarzyna.shakier.AppFlow.Onboarding;
import katarzyna.shakier.Database.AppDatabase;
import katarzyna.shakier.Database.DAOs.WordsDao;
import katarzyna.shakier.Database.Models.Words;
import katarzyna.shakier.Helpers.Utils;

public class MainActivity extends AppCompatActivity
{
    private static long backPressed;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CaocConfig.Builder.create()
                .backgroundMode(CaocConfig.BACKGROUND_MODE_SHOW_CUSTOM)
                .enabled(true)
                .showErrorDetails(false)
                .showRestartButton(false)
                .logErrorOnRestart(false)
                .trackActivities(true)
                .minTimeBetweenCrashesMs(2000)
                .errorDrawable(null)
                .restartActivity(null) //default: null (your app's launch activity)
                .errorActivity(ErrorPage.class) //default: null (default error activity)
                .eventListener(null)
                .apply();
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        showTutorialToNewUser();
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    public void showTutorialToNewUser()
    {
        boolean isUserFirstTime = Utils.readSharedBoolSetting(MainActivity.this, getString(R.string.PREF_USER_FIRST_TIME), true);

        if(isUserFirstTime)
        {
            try
            {
                new InitializeDatabase().execute().get();
            }
            catch (InterruptedException | ExecutionException e)
            {
                e.printStackTrace();
            }

            Utils.saveSharedBoolSetting(this, getString(R.string.PREF_USER_FIRST_TIME), false);
            Intent openOnboarding= new Intent(this, Onboarding.class);
            openOnboarding.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivityIfNeeded(openOnboarding, 0);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu)
    {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.menuAboutApp:
            {
                AlertDialog dialog =new AlertDialog.Builder(this)
                        .setTitle(R.string.aboutApp)
                        .setMessage(R.string.aboutAppText)
                        .setIcon(R.drawable.about_app)
                        .setCancelable(true)
                        .create();
                dialog.show();
                ((TextView)Objects.requireNonNull(dialog.findViewById(android.R.id.message))).setMovementMethod(LinkMovementMethod.getInstance());
                return true;
            }
            case R.id.menuTutorial:
            {
                startActivity(new Intent(this, Onboarding.class));
                return true;
            }
            case R.id.menuResetData:
            {
                new AlertDialog.Builder(this)
                        .setTitle(R.string.resetDataDialog)
                        .setMessage(R.string.resetDataDescription)
                        .setIcon(R.drawable.clear_db)
                        .setCancelable(true)
                        .setPositiveButton(R.string.YesButton, new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                Utils.resetSharedSettingsToDefault(getApplicationContext());
                                ((ActivityManager)Objects.requireNonNull(getSystemService(ACTIVITY_SERVICE))).clearApplicationUserData();
                            }
                        })
                        .setNegativeButton(R.string.NoButton, new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                Toast.makeText(getApplicationContext(), getString(R.string.noReset), Toast.LENGTH_SHORT).show();
                            }
                        })
                        .show();
                return true;
            }
            case R.id.menuAboutDev:
            {
                Utils.saveSharedBoolSetting(getApplicationContext(), getString(R.string.DEVELOPER_SITE_CLICK),true);
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.developerSite))));
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onCardClicked(View view)
    {
        switch (view.getId())
        {
            case R.id.cardViewAlgorithms:
            {
                startActivity(new Intent(this, AlgorithmsActivity.class));
                break;
            }
            case R.id.cardViewStats:
            {
                startActivity(new Intent(this, ChooseStatisticsActivity.class));
                break;
            }
            case R.id.cardViewTrophies:
            {
                startActivity(new Intent(this, AchievementsActivity.class));
                break;
            }
        }
    }

    @Override
    public void onBackPressed()
    {
        Toast.makeText(getApplicationContext(), R.string.doubleBackPressToExit, Toast.LENGTH_SHORT).show();
        if (backPressed + R.integer.backPressedInterval > System.currentTimeMillis())
        {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        else
        backPressed = System.currentTimeMillis();
    }

    private class InitializeDatabase extends AsyncTask<Void, Void, Void>
    {
        @Override
        protected Void doInBackground(Void... voids)
        {
            //This code is retarded, because Google engineers couldn't think of initialize a db without making a full query...
            AppDatabase appDatabase= AppDatabase.getInstance(getApplicationContext());
            WordsDao wordsDao = appDatabase.wordsDao();
            List<Words> wordsList = wordsDao.selectAmount(1);
            return null;
        }
    }
}
