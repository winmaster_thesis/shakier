package katarzyna.shakier.Helpers;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import katarzyna.shakier.R;

public class CustomStatsRow extends RelativeLayout
{
    Context context;
    View rootView;
    TextView textViewStatName;
    TextView textViewStatValue;

    public CustomStatsRow(Context context)
    {
        super(context);
        this.context = context;
        Init(context);
    }

    public CustomStatsRow(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        this.context = context;
        Init(context);
    }

    public void Init(Context context)
    {
        rootView = inflate(context, R.layout.stats_row, this);

        textViewStatName = rootView.findViewById(R.id.textViewStatName);
        textViewStatValue = rootView.findViewById(R.id.textViewStatValue);
    }

    public void Builder(String statTitle, String statValue, String valueUnitSuffix)
    {
        textViewStatName.setText(statTitle);
        textViewStatValue.setText(String.format("%s%s", statValue, valueUnitSuffix));
    }
}
