package katarzyna.shakier;

import java.util.ArrayList;
import java.util.Random;

public class SortingTestHelper
{
    static Random random= new Random();

    public static ArrayList<Integer> populateIntData(int amount)
    {
        ArrayList<Integer> list = new ArrayList<>();

        for(int i=0; i<amount; i++)
        {
            list.add(random.nextInt(10000));
        }
        return list;
    }

    public static ArrayList<String> populateStringData(int amount)
    {
        ArrayList<String> stringListToSort = new ArrayList<>();
        for(int i=0; i<amount; i++)
        {
            StringBuilder value = new StringBuilder();
            int length = random.nextInt(5)+2;

            for(int j=0; j<length; j++)
            {
                char character = (char) (random.nextInt(25)+97);
                value.append(character);
            }

            stringListToSort.add(value.toString());
        }
        return  stringListToSort;
    }
}
