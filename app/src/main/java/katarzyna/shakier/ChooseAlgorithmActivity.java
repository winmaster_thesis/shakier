package katarzyna.shakier;

import android.app.ActivityOptions;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;

public class ChooseAlgorithmActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_algorithm);
    }

    public void onAlgorithmCardViewClick(View view)
    {
        switch(view.getId())
        {
            case R.id.cardViewBubbleSort:
            {
                ActivityOptions transitionActivityOptions = ActivityOptions.makeSceneTransitionAnimation(
                        ChooseAlgorithmActivity.this,
                        Pair.create(findViewById(R.id.imageViewBubbleSort),
                                "sharedLogoTransition"));
                startActivity(new Intent(ChooseAlgorithmActivity.this,
                        AlgorithmsInfoActivity.class).putExtra(getString(R.string.algorithmName),
                        getString(R.string.algBubbleSort)), transitionActivityOptions.toBundle());
                break;
            }
            case R.id.cardViewInsertSort:
            {
                ActivityOptions transitionActivityOptions = ActivityOptions.makeSceneTransitionAnimation(
                        ChooseAlgorithmActivity.this,
                        Pair.create(findViewById(R.id.imageViewInsertSort),
                                "sharedLogoTransition"));
                startActivity(new Intent(ChooseAlgorithmActivity.this,
                        AlgorithmsInfoActivity.class).putExtra(getString(R.string.algorithmName),
                        getString(R.string.algInsertSort)), transitionActivityOptions.toBundle());
                break;
            }
            case R.id.cardViewSelectSort:
            {
                ActivityOptions transitionActivityOptions = ActivityOptions.makeSceneTransitionAnimation(
                        ChooseAlgorithmActivity.this,
                        Pair.create(findViewById(R.id.imageViewSelectSort),
                                "sharedLogoTransition"));
                startActivity(new Intent(ChooseAlgorithmActivity.this,
                        AlgorithmsInfoActivity.class).putExtra(getString(R.string.algorithmName),
                        getString(R.string.algSelectSort)), transitionActivityOptions.toBundle());
                break;
            }
            case R.id.cardViewBogoSort:
            {
                ActivityOptions transitionActivityOptions = ActivityOptions.makeSceneTransitionAnimation(
                        ChooseAlgorithmActivity.this,
                        Pair.create(findViewById(R.id.imageViewBogoSort),
                                "sharedLogoTransition"));
                startActivity(new Intent(ChooseAlgorithmActivity.this,
                        AlgorithmsInfoActivity.class).putExtra(getString(R.string.algorithmName),
                        getString(R.string.algBogoSort)), transitionActivityOptions.toBundle());
                break;
            }
            case R.id.cardViewGnomeSort:
            {
                ActivityOptions transitionActivityOptions = ActivityOptions.makeSceneTransitionAnimation(
                        ChooseAlgorithmActivity.this,
                        Pair.create(findViewById(R.id.imageViewGnomeSort),
                                "sharedLogoTransition"));
                startActivity(new Intent(ChooseAlgorithmActivity.this,
                        AlgorithmsInfoActivity.class).putExtra(getString(R.string.algorithmName),
                        getString(R.string.algGnomeSort)), transitionActivityOptions.toBundle());
                break;
            }
        }
    }
}
