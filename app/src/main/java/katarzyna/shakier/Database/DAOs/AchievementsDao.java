package katarzyna.shakier.Database.DAOs;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import katarzyna.shakier.Database.Models.Achievements;

@Dao
public interface AchievementsDao
{
    @Query("SELECT * FROM Achievements")
    List<Achievements> selectAll();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAchievement(Achievements achievement);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAll(Achievements... achievements);
}
