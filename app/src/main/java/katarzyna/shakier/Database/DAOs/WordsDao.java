package katarzyna.shakier.Database.DAOs;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import katarzyna.shakier.Database.Models.Words;

@Dao
public interface WordsDao
{
    @Query("SELECT * FROM Words LIMIT :limit")
    List<Words> selectAmount(int limit);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertWord(Words word);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAll(Words... words);
}
