package katarzyna.shakier.Database.DAOs;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import katarzyna.shakier.Database.Models.Algorithms;

@Dao
public interface AlgorithmsDao
{
    @Query("SELECT * FROM Algorithms")
    List<Algorithms> selectAll();

    @Query("SELECT * FROM Algorithms WHERE AlgorithmName LIKE :algorithmName LIMIT 1")
    Algorithms findByName(String algorithmName);

    @Query("SELECT AlgorithmName FROM Algorithms")
    List<String> selectAllAlgorithmNames();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAlgorithm(Algorithms algorithm);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAll(Algorithms... algorithms);
}
