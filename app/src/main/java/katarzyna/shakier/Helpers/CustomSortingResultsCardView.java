package katarzyna.shakier.Helpers;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import katarzyna.shakier.R;

public class CustomSortingResultsCardView extends RelativeLayout
{
    Context context;
    View rootView;
    CardView cardViewContainer;
    TextView textViewAlgorithmName;
    TextView textViewSortingResultTime;
    ImageView imageViewSortingResult;

    public CustomSortingResultsCardView(Context context)
    {
        super(context);
        this.context = context;
        Init(context);
    }

    public CustomSortingResultsCardView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        this.context = context;
        Init(context);
    }

    private void Init(Context context)
    {
        rootView = inflate(context, R.layout.custom_sorting_result_cardview, this);

        cardViewContainer = rootView.findViewById(R.id.cardViewSortingResult);
        textViewAlgorithmName = rootView.findViewById(R.id.textViewAlgorithmName);
        textViewSortingResultTime = rootView.findViewById(R.id.textViewSortingResultTime);
        imageViewSortingResult = rootView.findViewById(R.id.imageViewIcon);
    }

    public void Builder(String algorithmName, long resultTime, Drawable image, AlgorithmWarsResults algorithmWarsResult)
    {

        switch (algorithmWarsResult)
        {
            case LOOSER:
            {
                cardViewContainer.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                break;
            }
            case WINNER:
            {
                cardViewContainer.setBackgroundColor(getResources().getColor(R.color.colorAccentRev));
                break;
            }
        }

        textViewAlgorithmName.setText(algorithmName);
        textViewSortingResultTime.setText(String.format("%s%s%s", getResources().getString(R.string.algorithmWarsTimer), resultTime, getResources().getString(R.string.timerUnit)));
        imageViewSortingResult.setImageDrawable(image);
    }
}
