package katarzyna.shakier;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igalata.bubblepicker.BubblePickerListener;
import com.igalata.bubblepicker.adapter.BubblePickerAdapter;
import com.igalata.bubblepicker.model.BubbleGradient;
import com.igalata.bubblepicker.model.PickerItem;
import com.igalata.bubblepicker.rendering.BubblePicker;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import katarzyna.shakier.Database.AppDatabase;
import katarzyna.shakier.Database.DAOs.AlgorithmsDao;
import katarzyna.shakier.Database.DAOs.SortingResultsDao;
import katarzyna.shakier.Helpers.CustomStatsRow;
import katarzyna.shakier.Helpers.Utils;

public class StatisticsActivity extends AppCompatActivity
{
    SortingResultsDao sortingResultsDao;
    AlgorithmsDao algorithmsDao;
    ArrayList<String> availableAlgorithms;
    ArrayList<String> startStatsNames;
    ArrayList<String> algorithmStatsNames;
    ArrayList<StatisticValue> startStatsValues;
    ArrayList<StatisticValue> algorithmStatsValues;

    BubblePicker picker;
    TextView textViewStatsTitle;
    ImageView imageViewLogo;
    LinearLayout layoutStats;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        sortingResultsDao = AppDatabase.getInstance(getApplicationContext()).sortingResultsDao();
        algorithmsDao = AppDatabase.getInstance(getApplicationContext()).algorithmsDao();

        availableAlgorithms = (ArrayList<String>) algorithmsDao.selectAllAlgorithmNames();

        picker = findViewById(R.id.picker);
        layoutStats = findViewById(R.id.layoutStats);
        textViewStatsTitle = findViewById(R.id.textViewStatsTitle);
        imageViewLogo = findViewById(R.id.imageViewLogo);

        setupStatNames();
        setStartUpStatistics();
        showGlobalStatistics();

        setupPicker();

        checkSortingPeriodForAchievement();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        picker.onResume();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        picker.onPause();
    }

    private void setupStatNames()
    {
        startStatsNames = new ArrayList<>();
        startStatsNames.add(getString(R.string.statsAmountOfResults));
        startStatsNames.add(getString(R.string.statsAvgSortingTime));
        startStatsNames.add(getString(R.string.statsSumSoringTime));
        startStatsNames.add(getString(R.string.statsMinSoringTime));
        startStatsNames.add(getString(R.string.statsMaxSoringTime));
        startStatsNames.add(getString(R.string.statsAvgSortingDatasetSize));

        algorithmStatsNames = new ArrayList<>();
        algorithmStatsNames.add(getString(R.string.statsAmountOfResults));
        algorithmStatsNames.add(getString(R.string.statsAvgSortingTime));
        algorithmStatsNames.add(getString(R.string.statsSumSoringTime));
        algorithmStatsNames.add(getString(R.string.statsMinSoringTime));
        algorithmStatsNames.add(getString(R.string.statsMaxSoringTime));
        algorithmStatsNames.add(getString(R.string.statsPercentageAmount));
    }

    private void setStartUpStatistics()
    {
        startStatsValues = new ArrayList<>();
        startStatsValues.add(new StatisticValue(sortingResultsDao.getAmountOfResults(), ""));
        startStatsValues.add(new StatisticValue(Utils.round(sortingResultsDao.getAverageSortingDurationTime(),getResources().getInteger(R.integer.roundToPlace)),getString(R.string.timerUnit)));
        startStatsValues.add(new StatisticValue(sortingResultsDao.getSumOfAllSortingDurationTime(),getString(R.string.timerUnit)));
        startStatsValues.add(new StatisticValue(sortingResultsDao.getMinOfAllSortingTimes(),getString(R.string.timerUnit)));
        startStatsValues.add(new StatisticValue(sortingResultsDao.getMaxOfAllSortingTimes(), getString(R.string.timerUnit)));
        startStatsValues.add(new StatisticValue(Utils.round(sortingResultsDao.getAverageOfAllSortingSetSize(),getResources().getInteger(R.integer.roundToPlace)), ""));
    }

    private void showGlobalStatistics()
    {
        layoutStats.removeAllViewsInLayout();
        for(int i=0; i<startStatsNames.size(); i++)
        {
            CustomStatsRow customStatsRow = new CustomStatsRow(getApplicationContext());
            customStatsRow.Builder(startStatsNames.get(i), startStatsValues.get(i).statValue.toString(), startStatsValues.get(i).statUnit);

            layoutStats.addView(customStatsRow);
            layoutStats.refreshDrawableState();
        }
    }

    private void setupPicker()
    {
        picker.setBubbleSize(getResources().getInteger(R.integer.pickerBubbleSize));
        picker.setMaxSelectedCount(1);
        picker.setCenterImmediately(true);
        picker.setAdapter(new BubblePickerAdapter() {
            @Override
            public int getTotalCount() {
                return availableAlgorithms.size();
            }

            @NotNull
            @Override
            public PickerItem getItem(int position) {
                PickerItem item = new PickerItem();
                item.setTitle(availableAlgorithms.get(position));
                item.setGradient(new BubbleGradient(getResources().getColor(R.color.colorBetweenPrimary),
                        getResources().getColor(R.color.colorPrimaryDark), BubbleGradient.HORIZONTAL));
                item.setTextColor(getResources().getColor(R.color.colorDisabled));
                item.setBackgroundImage(getDrawable(getApplicationContext().getResources().getIdentifier(Utils.returnIcoAssetNameByAlgorithmName(availableAlgorithms.get(position)), "drawable", getApplicationContext().getPackageName())));
                return item;
            }
        });

        picker.setListener(new BubblePickerListener() {
            @Override
            public void onBubbleSelected(@NotNull PickerItem item)
            {
                showStatisticForAlgorithm(item.getTitle());
                textViewStatsTitle.setText(item.getTitle());
                imageViewLogo.setImageDrawable(item.getBackgroundImage());
            }

            @Override
            public void onBubbleDeselected(@NotNull PickerItem item)
            {
                showGlobalStatistics();
                textViewStatsTitle.setText(getString(R.string.textStatsGlobal));
                imageViewLogo.setImageDrawable(getDrawable(R.drawable.ic_app_logo_round));
            }
        });
    }

    private void showStatisticForAlgorithm(String algName)
    {
        try {
            algorithmStatsValues = new LoadStatisticForAlgorithm(algName).execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        layoutStats.removeAllViewsInLayout();
        for(int i=0; i<algorithmStatsNames.size(); i++)
        {
            CustomStatsRow customStatsRow = new CustomStatsRow(getApplicationContext());
            customStatsRow.Builder(algorithmStatsNames.get(i), algorithmStatsValues.get(i).statValue.toString(), algorithmStatsValues.get(i).statUnit);

            layoutStats.addView(customStatsRow);
            layoutStats.refreshDrawableState();
        }
    }

    private void checkSortingPeriodForAchievement()
    {
        if((long)startStatsValues.get(2).statValue > getResources().getInteger(R.integer.LONG_SORTING_TIME_PERIOD))
        {
            Utils.saveSharedBoolSetting(getApplicationContext(), getString(R.string.LONG_SORTING_TIME),true);
        }
    }

    private class LoadStatisticForAlgorithm extends AsyncTask<Void, Void, ArrayList<StatisticValue>>
    {
        String algorithmName;

        LoadStatisticForAlgorithm(String algName)
        {
            algorithmName = algName;
        }

        @Override
        protected ArrayList<StatisticValue> doInBackground(Void...voids)
        {
            ArrayList<StatisticValue> values = new ArrayList<>();
            values.add(new StatisticValue(sortingResultsDao.getAmountOfResultsForAlgorithm(algorithmName), ""));
            values.add(new StatisticValue(Utils.round(sortingResultsDao.getAverageSortingDurationForAlgorithm(algorithmName),getResources().getInteger(R.integer.roundToPlace))
                        ,getString(R.string.timerUnit)));
            values.add(new StatisticValue(sortingResultsDao.getSumOfSortingDurationTimeForAlgorithm(algorithmName),getString(R.string.timerUnit)));
            values.add(new StatisticValue(sortingResultsDao.getMinOfSortingDurationTimeForAlgorithm(algorithmName),getString(R.string.timerUnit)));
            values.add(new StatisticValue(sortingResultsDao.getMaxOfSortingDurationTimeForAlgorithm(algorithmName), getString(R.string.timerUnit)));

            double percentageResult = (double) (int)values.get(0).statValue / (int) startStatsValues.get(0).statValue;
            percentageResult = percentageResult * 100;

            values.add(new StatisticValue(Utils.round(percentageResult,getResources().getInteger(R.integer.roundToPlace)), getString(R.string.percentage)));

            return values;
        }
    }

    private class StatisticValue<T>
    {
        T statValue;
        String statUnit;

        StatisticValue(T statValue, String statUnit)
        {
            this.statValue = statValue;
            this.statUnit = statUnit;
        }
    }
}
