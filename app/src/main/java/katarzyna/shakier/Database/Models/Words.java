package katarzyna.shakier.Database.Models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.util.Log;

@Entity
public class Words
{
    @PrimaryKey()
    @NonNull
    @ColumnInfo(name="Word")
    private String word;

    public Words(String word) {
        this.word = word;
    }

    public String getWord()
    {
        return word;
    }
    public void setWord(String word)
    {
        this.word = word;
    }

    public static Words[] populateData() {
        Log.d("DATABASE", "---------------------------------------------------------->Words");
       return PopulateHelper.getWords();
    }
}
