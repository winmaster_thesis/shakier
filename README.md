# App is now on Google Play! (beta test) :heart:
<a href='https://play.google.com/store/apps/details?id=katarzyna.shakier&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img width="250" height="100" alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png'/></a>
![](/screens/promotion.png)
![](/screens/qr.png)

# About application

Shakier is my engineering work, made by student for all students, who want to learn more about sorting algorithms. 

App has two modes for learning: 

- step by step
- algorithm duels. 

Data about datasets and sorting duration times are collected in local database to generate chart and statistics. :chart_with_upwards_trend:

User, who actively participates in learning process, unlocks some achievements to reward mental effort. :trophy:

App has lots of animations, uses sensors and gestures for better memorize. User is guided by tips and onboarding tutorials on every step of using this application. I hope it will be usefull and handy in exams preparations. :school_satchel:

Project will be further developed and more features will be added. :muscle:

# Screens
![](/screens/screen1.jpg)
![](/screens/screen2.jpg)
![](/screens/screen3.jpg)
![](/screens/screen4.jpg)
![](/screens/screen5.jpg)
![](/screens/screen6.jpg)
![](/screens/screen7.jpg)
![](/screens/screen8.jpg)


## New features, that I plan in next version
- [ ] Achievement system based on Google Play Game Services
- [ ] Small improvements of steering in step by step activity
- [ ] "Going back in time" in step by step acitivity, to return to one of previous step of sorting
- [ ] Benchmark to adjust sorting dataset bounds, according to device computing power in algorithm duels to get satisfying results

## Special Thanks
Special thanks to authors of all plugins, libraries, icons and graphics, that I used in my project. It  made my app so beautiful. See [CREDITS](/CREDITS.md) file for details.