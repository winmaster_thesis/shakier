package katarzyna.shakier.Database.Models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class SortingResults implements Comparable
{
    @PrimaryKey(autoGenerate = true)
    private int resultId;

    @ColumnInfo(name="AlgorithmName")
    private String algorithmName;

    @ColumnInfo(name="SortingSetSize")
    private int sortingSetSize;

    @ColumnInfo(name="SortingDurationTime")
    private long sortingDurationTime;

    public int getResultId() {
        return resultId;
    }

    public void setResultId(int resultId) {
        this.resultId = resultId;
    }

    public String getAlgorithmName() {
        return algorithmName;
    }

    public void setAlgorithmName(String algorithmName) {
        this.algorithmName = algorithmName;
    }

    public int getSortingSetSize() {
        return sortingSetSize;
    }

    public void setSortingSetSize(int sortingSetSize) {
        this.sortingSetSize = sortingSetSize;
    }

    public long getSortingDurationTime() {
        return sortingDurationTime;
    }

    public void setSortingDurationTime(long sortingDurationTime) {
        this.sortingDurationTime = sortingDurationTime;
    }

    public SortingResults(String algorithmName, int sortingSetSize, long sortingDurationTime)
    {
        this.algorithmName = algorithmName;
        this.sortingSetSize = sortingSetSize;
        this.sortingDurationTime = sortingDurationTime;
    }

    @Override
    public int compareTo(@NonNull Object sortingResult)
    {
        if(sortingResult instanceof  SortingResults)
        {
            SortingResults object = (SortingResults) sortingResult;
            return  (Integer)this.sortingSetSize < (Integer)object.sortingSetSize ? -1 : ((Integer) this.sortingSetSize).equals((Integer) object.sortingSetSize) ? 0 : 1;
        }
        return  -1;
    }
}
