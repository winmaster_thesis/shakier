package katarzyna.shakier;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class AlgorithmsActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_algorithms);
    }

    public void onAlgorithmsModeClick(View view)
    {
        if(view.getId() == R.id.cardViewStepByStep)
        {
            startActivity(new Intent(this, ChooseAlgorithmActivity.class));
        }
        if(view.getId() == R.id.cardViewAlgorithmWars)
        {
            startActivity(new Intent(this, DataGenerationAdvancedActivity.class));
        }
    }
}
