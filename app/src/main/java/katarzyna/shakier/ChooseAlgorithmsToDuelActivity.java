package katarzyna.shakier;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

import java.util.ArrayList;

public class ChooseAlgorithmsToDuelActivity extends AppCompatActivity
{
    CardView cardViewBogoSort;
    CheckBox checkBoxBubbleSort;
    CheckBox checkBoxInsertSort;
    CheckBox checkBoxSelectSort;
    CheckBox checkBoxBogoSort;
    CheckBox checkBoxGnomeSort;
    ArrayList<String> chosenAlgorithms;
    ArrayList<CheckBox> checkBoxes;

    Bundle bundleData;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_algorithms_to_duel);

        cardViewBogoSort = findViewById(R.id.cardViewBogoSort);
        checkBoxBubbleSort = findViewById(R.id.checkBoxBubbleSort);
        checkBoxInsertSort = findViewById(R.id.checkBoxInsertSort);
        checkBoxSelectSort = findViewById(R.id.checkBoxSelectSort);
        checkBoxBogoSort = findViewById(R.id.checkBoxBogoSort);
        checkBoxGnomeSort = findViewById(R.id.checkBoxGnomeSort);

        checkBoxes = new ArrayList<>();
        checkBoxes.add(checkBoxBubbleSort);
        checkBoxes.add(checkBoxInsertSort);
        checkBoxes.add(checkBoxSelectSort);
        checkBoxes.add(checkBoxBogoSort);
        checkBoxes.add(checkBoxGnomeSort);
        chosenAlgorithms = new ArrayList<>();

        bundleData = getIntent().getExtras();
        checkBogoSortConstraint();
    }

    private void checkBogoSortConstraint()
    {
        if(bundleData.getInt(getString(R.string.listSize)) > getResources().getInteger(R.integer.bogoSortConstraintAmount))
        {
            checkBoxBogoSort.setEnabled(false);
            cardViewBogoSort.setBackgroundColor(getResources().getColor(R.color.colorDisabled));
            cardViewBogoSort.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new AlertDialog.Builder(view.getContext())
                            .setTitle(R.string.bogoSortConstraintAlertTitle)
                            .setMessage(R.string.bogoSortConstraintAlertContent)
                            .setIcon(R.drawable.bogo)
                            .setCancelable(true)
                            .setPositiveButton(R.string.YesButton, new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which)
                                {
                                    onBackPressed();
                                }
                            })
                            .setNegativeButton(R.string.NoButton, new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which)
                                {

                                }
                            })
                            .show();
                }
            });
        }
    }

    public void goToDuel(View view)
    {
        chosenAlgorithms.clear();
        for(CheckBox checkBox : checkBoxes)
        {
            if(checkBox.isChecked())
            {
                chosenAlgorithms.add(checkBox.getContentDescription().toString());
            }
        }

        if(chosenAlgorithms.size() >= getResources().getInteger(R.integer.algorithmWarsMinimumAlgorithmsChosen))
        {
            startActivity(new Intent(ChooseAlgorithmsToDuelActivity.this, AlgorithmWarsActivity.class)
                    .putExtra(getString(R.string.algorithmsSet), chosenAlgorithms)
                    .putExtra(getString(R.string.dataType), bundleData.getString(getString(R.string.dataType)))
                    .putExtra(getString(R.string.generatedListToSort), bundleData.getParcelableArrayList(getString(R.string.generatedListToSort)))
                    .putExtra(getString(R.string.sortOrder), bundleData.getBoolean(getString(R.string.sortOrder))));
        }
        else
        {
            Toast.makeText(getApplicationContext(), getString(R.string.algorithmAmountConstraint), Toast.LENGTH_SHORT).show();
        }
    }

    public void cardViewClickToSetCheckBox(View view)
    {
        switch (view.getId())
        {
            case R.id.cardViewBubbleSort:
            {
                if (checkBoxBubbleSort.isChecked())
                {
                    checkBoxBubbleSort.setChecked(false);
                } else {
                    checkBoxBubbleSort.setChecked(true);
                }
                break;
            }
            case R.id.cardViewInsertSort:
            {
                if (checkBoxInsertSort.isChecked())
                {
                    checkBoxInsertSort.setChecked(false);
                } else {
                    checkBoxInsertSort.setChecked(true);
                }
                break;
            }
            case R.id.cardViewSelectSort:
            {
                if (checkBoxSelectSort.isChecked())
                {
                    checkBoxSelectSort.setChecked(false);
                } else {
                    checkBoxSelectSort.setChecked(true);
                }
                break;
            }
            case R.id.cardViewBogoSort:
            {
                if (checkBoxBogoSort.isChecked())
                {
                    checkBoxBogoSort.setChecked(false);
                } else {
                    checkBoxBogoSort.setChecked(true);
                }
                break;
            }
            case R.id.cardViewGnomeSort:
            {
                if (checkBoxGnomeSort.isChecked())
                {
                    checkBoxGnomeSort.setChecked(false);
                } else {
                    checkBoxGnomeSort.setChecked(true);
                }
                break;
            }
        }
    }
}
