package katarzyna.shakier;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import katarzyna.shakier.Database.AppDatabase;
import katarzyna.shakier.Database.DAOs.WordsDao;
import katarzyna.shakier.Database.Models.Words;
import katarzyna.shakier.Helpers.Utils;

public class DataGenerationAdvancedActivity extends AppCompatActivity
{

    RadioGroup radioGroupDataChoice;
    RadioGroup radioGroupSortOrder;
    RadioButton radioButtonNumbers;
    RadioButton radioButtonWords;
    RadioButton radioButtonDescending;
    RadioButton radioButtonAscending;
    ListView listViewResults;
    SeekBar seekBarAmount;
    TextView textViewSeekbarTip;

    Random random;
    WordsDao wordsDao;

    ArrayList<Integer> numbers;
    ArrayList<String>words;

    int range;
    int maxRandomNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_generation_advanced);

        radioGroupDataChoice = findViewById(R.id.RadioGroupDataChoice);
        radioGroupSortOrder = findViewById(R.id.RadioGroupSortOrder);
        radioButtonNumbers = findViewById(R.id.radioButtonNumbers);
        radioButtonWords = findViewById(R.id.radioButtonWords);
        radioButtonDescending = findViewById(R.id.radioButtonDescending);
        radioButtonAscending = findViewById(R.id.radioButtonAscending);
        listViewResults = findViewById(R.id.listOfResults);
        seekBarAmount = findViewById(R.id.seekBarAmount);
        textViewSeekbarTip = findViewById(R.id.textViewSeekbarTip);

        radioGroupDataChoice.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i)
            {
                if(radioButtonWords.isChecked())
                {
                    seekBarAmount.setMax(getResources().getInteger(R.integer.globalLimitOfWords));
                }
                else {
                    seekBarAmount.setMax(getResources().getInteger(R.integer.algorithmWarsMaxAmountRange));
                }
                populateData();
            }
        });

        random = new Random();
        wordsDao = AppDatabase.getInstance(getApplicationContext()).wordsDao();
        numbers = new ArrayList<>();
        words = new ArrayList<>();
        maxRandomNumber = getResources().getInteger(R.integer.globalRandomMaxRange);
        range = getResources().getInteger(R.integer.algorithmWarsMinAmountRange);
        textViewSeekbarTip.setText(getString(R.string.seekBarAmountTip)+ "  " +  range);

        radioButtonNumbers.setChecked(true);
        radioButtonAscending.setChecked(true);

        seekBarAmount.setProgress(range);
        seekBarAmount.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b)
            {
                if(seekBar.getProgress() < getResources().getInteger(R.integer.algorithmWarsMinAmountRange))
                {
                    seekBar.setProgress(getResources().getInteger(R.integer.algorithmWarsMinAmountRange));
                }

                range = seekBar.getProgress();
                populateData();
                textViewSeekbarTip.setText(getString(R.string.seekBarAmountTip)+ "  " + range);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    private void populateData()
    {
        numbers.clear();
        words.clear();

        if(radioButtonNumbers.isChecked())
        {
            for(int i=0; i<range; i++)
            {
                numbers.add(random.nextInt(maxRandomNumber));
            }
            listViewResults.setAdapter( new ArrayAdapter<>(this, R.layout.list_row, R.id.rowText, numbers));
        }
        if(radioButtonWords.isChecked())
        {
            words = Utils.convertWordsToStringArray((ArrayList<Words>) wordsDao.selectAmount(range));
            Collections.shuffle(words);
            listViewResults.setAdapter( new ArrayAdapter<>(this, R.layout.list_row, R.id.rowText, words));
        }
        listViewResults.setTextFilterEnabled(true);
    }


    public void actionGenerate(View view)
    {
        populateData();
    }

    public void goToSortingActivity(View view)
    {
        Intent nextIntent = new Intent(DataGenerationAdvancedActivity.this, ChooseAlgorithmsToDuelActivity.class);
        nextIntent.putExtra(getString(R.string.listSize), seekBarAmount.getProgress());

        if(radioButtonNumbers.isChecked())
        {
            nextIntent.putExtra(getString(R.string.dataType), getString(R.string.numbers));
            nextIntent.putExtra(getString(R.string.generatedListToSort), numbers);

            // false- ascending sorting ; true- descending sorting
            if(radioButtonAscending.isChecked())
            {
                nextIntent.putExtra(getString(R.string.sortOrder),false);
            }

            if(radioButtonDescending.isChecked())
            {
                nextIntent.putExtra(getString(R.string.sortOrder),true);
            }
        }
        if(radioButtonWords.isChecked())
        {
            nextIntent.putExtra(getString(R.string.dataType), getString(R.string.words));
            nextIntent.putExtra(getString(R.string.generatedListToSort), words);

            // false- ascending sorting ; true- descending sorting
            if(radioButtonAscending.isChecked())
            {
                nextIntent.putExtra(getString(R.string.sortOrder),false);
            }

            if(radioButtonDescending.isChecked())
            {
                nextIntent.putExtra(getString(R.string.sortOrder),true);
            }
        }
        startActivity(nextIntent);
    }

}
