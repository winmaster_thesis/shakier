package katarzyna.shakier;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.yy.mobile.rollingtextview.CharOrder;
import com.yy.mobile.rollingtextview.RollingTextView;
import com.yy.mobile.rollingtextview.strategy.Strategy;

import java.util.ArrayList;
import katarzyna.shakier.Helpers.Utils;
import katarzyna.sortPackage.Factory.Factory;
import katarzyna.sortPackage.Helpers.CompareMethod;
import katarzyna.sortPackage.Sorting.BaseSort;
import lecho.lib.hellocharts.view.ColumnChartView;
import uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetPrompt;

public class SortingStepsActivity extends AppCompatActivity implements SensorEventListener
{
    String algorithmName;
    boolean sortOrder;
    ArrayList valuesToSort;
    String dataType;

    ImageView imageViewLogo;
    TextView textViewTitle;
    TextView textViewSoringOrder;
    TextView textViewAmountSteps;
    TextView textViewCurrentStep;
    ListView listViewHistory;
    ArrayList<String> history;
    ArrayAdapter arrayAdapter;

    BaseSort sorter;
    ColumnChartView chart;
    RollingTextView rollingTextView;

    SensorManager mSensorManager;
    Sensor sensorAccelerometr;
    MediaPlayer mediaPlayer;

    float SHAKE_THRESHOLD = 2500;
    private long lastUpdate = 0;
    private float last_x, last_y, last_z;
    int iterationCount=0;
    long startTime=0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sorting_steps);

        Intent intent = getIntent();
        algorithmName = intent.getStringExtra(getString(R.string.algorithmName));
        // false- ascending sorting ; true- descending sorting
        sortOrder = intent.getBooleanExtra(getString(R.string.sortOrder),true);
        dataType = intent.getStringExtra(getString(R.string.dataType));
        getProperDataToSort(intent);
        history = new ArrayList<>();
        assignControls();

        Factory factory = new Factory(valuesToSort, sortOrder?  CompareMethod.DESCENDING : CompareMethod.ASCENDING);
        sorter = factory.produceSorter(algorithmName);

        if(dataType.equals(getString(R.string.words)))
        {
            chart.setVisibility(View.GONE);
            textViewCurrentStep.setVisibility(View.GONE);

            rollingTextView.setAnimationDuration(2000L);
            rollingTextView.setCharStrategy(Strategy.NormalAnimation());
            rollingTextView.addCharOrder(CharOrder.Alphabet);
            rollingTextView.setAnimationInterpolator(new AccelerateDecelerateInterpolator());
            rollingTextView.addAnimatorListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                }
            });
            rollingTextView.setText(sorter.getSortArray().toString());
        }
        else
        {
            chart.setColumnChartData(sorter.produceChartData());
            chart.setClickable(false);
            rollingTextView.setVisibility(View.GONE);
        }

        showTapTargetToNewUser();
        incrementSharedSettings();
        unlockAllStepsAchievementIfEnabled();
    }

    private void showTapTargetToNewUser()
    {
        boolean isUserFirstTime = Utils.readSharedBoolSetting(this, getString(R.string.PREF_USER_FIRST_TIME_STEP_BY_STEP), true);

        if(isUserFirstTime)
        {
            new MaterialTapTargetPrompt.Builder(this)
                    .setTarget(R.id.viewLine)
                    .setIconDrawable(getDrawable(R.drawable.shake))
                    .setPrimaryText(getString(R.string.targetShakePromptTitle))
                    .setSecondaryText(getString(R.string.targetShakePromptDscr))
                    .setPromptStateChangeListener(new MaterialTapTargetPrompt.PromptStateChangeListener() {
                        @Override
                        public void onPromptStateChanged(MaterialTapTargetPrompt prompt, int state) {
                        }
                    })
                    .show();

            Utils.saveSharedBoolSetting(this, getString(R.string.PREF_USER_FIRST_TIME_STEP_BY_STEP), false);
        }
    }

    private void assignControls()
    {
        imageViewLogo = findViewById(R.id.imageViewLogo);
        textViewTitle = findViewById(R.id.textViewTitle);
        textViewSoringOrder = findViewById(R.id.textViewSortingOrder);
        textViewAmountSteps = findViewById(R.id.textViewAmountSteps);
        textViewCurrentStep = findViewById(R.id.textViewCurrentStep);
        listViewHistory = findViewById(R.id.listViewHistory);
        arrayAdapter = new ArrayAdapter<>(this, R.layout.list_row, R.id.rowText, history);
        listViewHistory.setAdapter(arrayAdapter);
        chart = findViewById(R.id.chart);
        rollingTextView = findViewById(R.id.alphaBetView);

        textViewTitle.setText(algorithmName);
        textViewSoringOrder.setText(sortOrder ? R.string.dscSortingOrder : R.string.ascSortingOrder);
        textViewCurrentStep.setText(valuesToSort.toString());

        int icoId = getApplicationContext().getResources().getIdentifier(Utils.returnIcoAssetNameByAlgorithmName(algorithmName), "drawable", getApplicationContext().getPackageName());
        imageViewLogo.setImageResource(icoId);

        mediaPlayer=MediaPlayer.create(getApplicationContext(), R.raw.step);
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensorAccelerometr = null;
        mSensorManager.registerListener( this, sensorAccelerometr, SensorManager.SENSOR_DELAY_NORMAL);

        if (mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null)
        {
            sensorAccelerometr = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        }
        else
        {
            Toast.makeText(getApplicationContext(), getString(R.string.noAccelometerInfo), Toast.LENGTH_LONG).show();
            onBackPressed();
        }

        addItemsToHistory();
        startTime=System.currentTimeMillis();
    }

    private void getProperDataToSort(Intent intent)
    {
        switch (dataType)
        {
            case "NUMBERS":
            {
                valuesToSort = intent.getIntegerArrayListExtra(getString(R.string.generatedListToSort));
                break;
            }
            case "WORDS":
            {
                valuesToSort = intent.getStringArrayListExtra(getString(R.string.generatedListToSort));
                break;
            }
        }
    }

    private void makeASortStep()
    {
        textViewAmountSteps.setText(getString(R.string.stepsCount) +" "+ iterationCount);
        if(sorter.isSorted())
        {
            mSensorManager.unregisterListener(this);
            mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.done);
            mediaPlayer.start();
            Toast.makeText(getApplicationContext(), getString(R.string.stepByStepFinished) + Utils.getSortingDurationTime(startTime, System.currentTimeMillis()), Toast.LENGTH_SHORT).show();
        }
        else
        {
            sorter.sortStep();
            chart.setColumnChartData(sorter.produceChartData());
            rollingTextView.setText(sorter.getSortArray().toString());
            textViewCurrentStep.setText(sorter.printSortArray());
        }
        addItemsToHistory();
    }

    private void addItemsToHistory()
    {
        history.add(textViewCurrentStep.getText().toString());
        arrayAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        mSensorManager.registerListener( this, sensorAccelerometr, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent)
    {
        Sensor mySensor = sensorEvent.sensor;
        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER)
        {
            float x = sensorEvent.values[0];
            float y = sensorEvent.values[1];
            float z = sensorEvent.values[2];

            long curTime = System.currentTimeMillis();

            if ((curTime - lastUpdate) > 100)
            {
                long diffTime = (curTime - lastUpdate);
                lastUpdate = curTime;

                float speed = Math.abs(x + y + z - last_x - last_y - last_z) / diffTime * 10000;

                if (speed > SHAKE_THRESHOLD)
                {
                    iterationCount++;
                    makeASortStep();
                    mediaPlayer.start();
                }

                last_x = x;
                last_y = y;
                last_z = z;
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i)
    {

    }

    private void incrementSharedSettings()
    {
        Utils.incrementSharedIntSetting(getApplicationContext(), Utils.returnSharedPrefNameByAlgorithmName(algorithmName));
    }

    private void unlockAllStepsAchievementIfEnabled()
    {
        if(Utils.completeAllStepByStepAchievement(getApplicationContext()))
        {
            Utils.saveSharedBoolSetting(getApplicationContext(), getString(R.string.COMPLETE_STEP_BY_STEP), true);
        }
    }
}
