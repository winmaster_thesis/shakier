package katarzyna.shakier.Database.Models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.util.Log;

@Entity
public class Algorithms
{
    @PrimaryKey()
    @NonNull
    @ColumnInfo(name="AlgorithmName")
    private String algorithmName;

    @ColumnInfo(name="AlgorithmStability")
    private String algorithmStability;

    @ColumnInfo(name="AlgorithmComplexity")
    private String algorithmComplexity;

    @ColumnInfo(name="AlgorithmStructure")
    private String algorithmStructure;

    @ColumnInfo(name="AlgorithmInformations")
    private String algorithmInformations;

    @ColumnInfo(name="IconName")
    private String iconName;

    public Algorithms(String algorithmName, String algorithmStability, String algorithmComplexity, String algorithmStructure, String algorithmInformations, String iconName) {
        this.algorithmName = algorithmName;
        this.algorithmStability = algorithmStability;
        this.algorithmComplexity = algorithmComplexity;
        this.algorithmStructure = algorithmStructure;
        this.algorithmInformations = algorithmInformations;
        this.iconName = iconName;
    }

    public String getAlgorithmName() {
        return algorithmName;
    }

    public void setAlgorithmName(String algorithmName) {
        this.algorithmName = algorithmName;
    }

    public String getAlgorithmStability() {
        return algorithmStability;
    }

    public void setAlgorithmStability(String algorithmStability) {
        this.algorithmStability = algorithmStability;
    }

    public String getAlgorithmComplexity() {
        return algorithmComplexity;
    }

    public void setAlgorithmComplexity(String algorithmComplexity) {
        this.algorithmComplexity = algorithmComplexity;
    }

    public String getAlgorithmStructure() {
        return algorithmStructure;
    }

    public void setAlgorithmStructure(String algorithmStructure) {
        this.algorithmStructure = algorithmStructure;
    }

    public String getAlgorithmInformations() {
        return algorithmInformations;
    }

    public void setAlgorithmInformations(String algorithmInformations) {
        this.algorithmInformations = algorithmInformations;
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public static Algorithms[] populateData() {
        Log.d("DATABASE", "---------------------------------------------------------->Algorithms");
        return  PopulateHelper.getAlgorithms();
    }
}
