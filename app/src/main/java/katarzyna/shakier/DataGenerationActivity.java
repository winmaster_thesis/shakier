package katarzyna.shakier;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.ArrayList;
import java.util.Random;

import katarzyna.shakier.Database.AppDatabase;
import katarzyna.shakier.Database.DAOs.WordsDao;
import katarzyna.shakier.Database.Models.Words;
import katarzyna.shakier.Helpers.Utils;

public class DataGenerationActivity extends AppCompatActivity
{
    RadioGroup radioGroupDataChoice;
    RadioGroup radioGroupSortOrder;
    RadioButton radioButtonNumbers;
    RadioButton radioButtonWords;
    RadioButton radioButtonDescending;
    RadioButton radioButtonAscending;
    ListView listViewResults;

    Random random;
    WordsDao wordsDao;

    ArrayList<Integer> numbers;
    ArrayList<String>words;

    Intent prevIntent;

    int range;
    int maxRandomNumber;
    int wordsRange;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_generation);
        prevIntent = getIntent();

        radioGroupDataChoice = findViewById(R.id.RadioGroupDataChoice);
        radioGroupSortOrder = findViewById(R.id.RadioGroupSortOrder);
        radioButtonNumbers = findViewById(R.id.radioButtonNumbers);
        radioButtonWords = findViewById(R.id.radioButtonWords);
        radioButtonDescending = findViewById(R.id.radioButtonDescending);
        radioButtonAscending = findViewById(R.id.radioButtonAscending);
        listViewResults = findViewById(R.id.listOfResults);
        radioGroupDataChoice.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i)
            {
                populateData();
            }
        });

        random = new Random();
        wordsDao = AppDatabase.getInstance(getApplicationContext()).wordsDao();
        numbers = new ArrayList<>();
        words = new ArrayList<>();

        radioButtonNumbers.setChecked(true);
        radioButtonAscending.setChecked(true);
    }

    private void populateData()
    {
        numbers.clear();
        words.clear();
        setRangesAndConstraints();

        if(radioButtonNumbers.isChecked())
        {
            for(int i=0; i<range; i++)
            {
                numbers.add(random.nextInt(maxRandomNumber));
            }
            listViewResults.setAdapter( new ArrayAdapter<>(this, R.layout.list_row, R.id.rowText, numbers));
        }
        if(radioButtonWords.isChecked())
        {
            ArrayList<String> newWords = Utils.convertWordsToStringArray((ArrayList<Words>) wordsDao.selectAmount(getResources().getInteger(R.integer.globalLimitOfWords)));

            for(int i=0; i<wordsRange; i++)
            {
                words.add(newWords.get(random.nextInt(newWords.size())));
            }
            listViewResults.setAdapter( new ArrayAdapter<>(this, R.layout.list_row, R.id.rowText, words));
        }
        listViewResults.setTextFilterEnabled(true);
    }

    private void setRangesAndConstraints()
    {
        range = random.nextInt(getResources().getInteger(R.integer.algorithmStepsMaxAmountRange))+getResources().getInteger(R.integer.algorithmStepsMinAmountRange);
        maxRandomNumber = getResources().getInteger(R.integer.stepByStepRandomMaxRange);
        wordsRange = getResources().getInteger(R.integer.stepByStepWordsMaxRange);
    }

    public void actionGenerate(View view)
    {
        populateData();
    }

    public void goToSortingActivity(View view)
    {
        Intent nextIntent;
        nextIntent= new Intent(DataGenerationActivity.this, SortingStepsActivity.class);
        String algorithmName = prevIntent.getStringExtra(getString(R.string.algorithmName));
        nextIntent.putExtra(getString(R.string.algorithmName), algorithmName);

        if(radioButtonNumbers.isChecked())
        {
            nextIntent.putExtra(getString(R.string.dataType), getString(R.string.numbers));
            nextIntent.putExtra(getString(R.string.generatedListToSort), numbers);

            // false- ascending sorting ; true- descending sorting
            if(radioButtonAscending.isChecked())
            {
                nextIntent.putExtra(getString(R.string.sortOrder),false);
            }

            if(radioButtonDescending.isChecked())
            {
                nextIntent.putExtra(getString(R.string.sortOrder),true);
            }
        }
        if(radioButtonWords.isChecked())
        {
            nextIntent.putExtra(getString(R.string.dataType), getString(R.string.words));
            nextIntent.putExtra(getString(R.string.generatedListToSort), words);

            // false- ascending sorting ; true- descending sorting
            if(radioButtonAscending.isChecked())
            {
                nextIntent.putExtra(getString(R.string.sortOrder),false);
            }

            if(radioButtonDescending.isChecked())
            {
                nextIntent.putExtra(getString(R.string.sortOrder),true);
            }
        }
        startActivity(nextIntent);
    }
}
