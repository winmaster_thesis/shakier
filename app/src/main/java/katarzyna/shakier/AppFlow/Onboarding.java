package katarzyna.shakier.AppFlow;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import com.github.paolorotolo.appintro.AppIntro2;
import com.github.paolorotolo.appintro.AppIntroFragment;

import katarzyna.shakier.MainActivity;
import katarzyna.shakier.R;

public class Onboarding extends AppIntro2
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        addSlide(AppIntroFragment.newInstance(getResources().getText(R.string.appIntroLearn),
                getResources().getText(R.string.appIntroLearnDscr),
                R.drawable.classic_alg,
                getResources().getColor(R.color.colorAccentRev)));
        addSlide(AppIntroFragment.newInstance(getResources().getText(R.string.appIntroWatch),
                getResources().getText(R.string.appIntroWatchDscr),
                R.drawable.stats,
                getResources().getColor(R.color.colorPrimaryDark)));
        addSlide(AppIntroFragment.newInstance(getResources().getText(R.string.appIntroAchieve),
                getResources().getText(R.string.appIntroAchieveDscr),
                R.drawable.achievements,
                getResources().getColor(R.color.colorPrimary)));
        addSlide(AppIntroFragment.newInstance(getResources().getText(R.string.appIntroDiscover),
                getResources().getText(R.string.appIntroDiscoverDscr),
                R.drawable.special_alg,
                getResources().getColor(R.color.colorAccent)));
        setFadeAnimation();
    }

    @Override
    public void onSkipPressed(Fragment currentFragment)
    {
        super.onSkipPressed(currentFragment);
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public void onDonePressed(Fragment currentFragment)
    {
        super.onDonePressed(currentFragment);
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment)
    {
        super.onSlideChanged(oldFragment, newFragment);
    }
}
