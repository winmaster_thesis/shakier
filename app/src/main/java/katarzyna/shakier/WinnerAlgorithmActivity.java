package katarzyna.shakier;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import katarzyna.shakier.Helpers.Utils;
import nl.dionsegijn.konfetti.KonfettiView;
import nl.dionsegijn.konfetti.models.Shape;
import nl.dionsegijn.konfetti.models.Size;

public class WinnerAlgorithmActivity extends AppCompatActivity
{
    CardView cardViewWinner;
    Animation animation;

    TextView textViewWinnerName;
    TextView textViewWinnerTime;
    ImageView imageViewWinner;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_winner_algorithm);

        cardViewWinner = findViewById(R.id.cardViewWinner);
        textViewWinnerName = findViewById(R.id.textViewWinnerName);
        textViewWinnerTime = findViewById(R.id.textViewWinnerTime);
        imageViewWinner = findViewById(R.id.imageViewWinner);


        setUpReceivedData();
        setUpConfettiAndAnimations();
    }

    private void setUpReceivedData()
    {
        Bundle bundle = getIntent().getExtras();
        String winnerName = bundle.getString(getString(R.string.algorithmName));
        textViewWinnerName.setText(winnerName);

        int icoId = getApplicationContext().getResources().getIdentifier(Utils.returnIcoAssetNameByAlgorithmName(winnerName),
                "drawable", getApplicationContext().getPackageName());

        imageViewWinner.setImageDrawable(getDrawable(icoId));

        textViewWinnerTime.setText(String.format(
                "%-10s%s%3s",
                getResources().getString(R.string.algorithmWarsTimer),
                bundle.getLong(getString(R.string.winnerTime)),
                getResources().getString(R.string.timerUnit)));
    }

    private void setUpConfettiAndAnimations()
    {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        KonfettiView viewKonfetti = findViewById(R.id.viewKonfetti);
        viewKonfetti.build()
                .addColors(getResources().getColor(R.color.colorPrimaryDark), getResources().getColor(R.color.colorPrimary),
                        getResources().getColor(R.color.colorAccent), getResources().getColor(R.color.colorAccentRev))
                .setDirection(0.0, metrics.heightPixels/2)
                .setSpeed(1f, 5f)
                .setFadeOutEnabled(true)
                .setTimeToLive(20000L)
                .addShapes(Shape.RECT, Shape.CIRCLE)
                .addSizes(new Size(12,12))
                .setPosition(metrics.widthPixels / 2, metrics.heightPixels /5)
                .stream(100, 5000L);

        animation = AnimationUtils.loadAnimation(this, R.anim.fade_and_zoom_anim);
        cardViewWinner.startAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                //nothing
            }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                Toast.makeText(getApplicationContext(), getString(R.string.toastClickToEnd), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                //nothing
            }
        });
    }

    public void onCardViewWinnerClick(View view)
    {
        startActivity(new Intent(this, MainActivity.class));
    }
}
