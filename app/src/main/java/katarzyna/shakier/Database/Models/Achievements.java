package katarzyna.shakier.Database.Models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.util.Log;

@Entity
public class Achievements
{
    @PrimaryKey()
    @NonNull
    @ColumnInfo(name="AchievementTitle")
    private String achievementTitle;

    @ColumnInfo(name="AchievementShortInfo")
    private String achievementShortInfo;

    @ColumnInfo(name="AchievementLongInfo")
    private String achievementLongInfo;

    @ColumnInfo(name="AchievementIcoName")
    private String achievementIcoName;

    @ColumnInfo(name="AchievementConnectedSharedPrefName")
    private String achievementConnectedSharedPrefName;

    @ColumnInfo(name="NumberOfIterations")
    private int  numberOfIterations; //0 means true/false


    public Achievements(String achievementTitle, String achievementShortInfo, String achievementLongInfo, String achievementIcoName, String achievementConnectedSharedPrefName, int numberOfIterations) {
        this.achievementTitle = achievementTitle;
        this.achievementShortInfo = achievementShortInfo;
        this.achievementLongInfo = achievementLongInfo;
        this.achievementIcoName = achievementIcoName;
        this.achievementConnectedSharedPrefName = achievementConnectedSharedPrefName;
        this.numberOfIterations = numberOfIterations;
    }

    public String getAchievementTitle() {
        return achievementTitle;
    }

    public void setAchievementTitle(String achievementTitle) {
        this.achievementTitle = achievementTitle;
    }

    public String getAchievementShortInfo() {
        return achievementShortInfo;
    }

    public void setAchievementShortInfo(String achievementShortInfo) {
        this.achievementShortInfo = achievementShortInfo;
    }

    public String getAchievementLongInfo() {
        return achievementLongInfo;
    }

    public void setAchievementLongInfo(String achievementLongInfo) {
        this.achievementLongInfo = achievementLongInfo;
    }

    public String getAchievementIcoName() {
        return achievementIcoName;
    }

    public void setAchievementIcoName(String achievementIcoName) {
        this.achievementIcoName = achievementIcoName;
    }

    public String getAchievementConnectedSharedPrefName() {
        return achievementConnectedSharedPrefName;
    }

    public void setAchievementConnectedSharedPrefName(String achievementConnectedSharedPrefName) {
        this.achievementConnectedSharedPrefName = achievementConnectedSharedPrefName;
    }

    public int getNumberOfIterations() {
        return numberOfIterations;
    }

    public void setNumberOfIterations(int numberOfIterations) {
        this.numberOfIterations = numberOfIterations;
    }

    public static Achievements[] populateData() {
        Log.d("DATABASE", "---------------------------------------------------------->Achivements");
        return  PopulateHelper.getAchievements();
    }
}
