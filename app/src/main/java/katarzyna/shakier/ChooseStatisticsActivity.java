package katarzyna.shakier;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import katarzyna.shakier.Helpers.Utils;

public class ChooseStatisticsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_statistics);
    }

    public void statisticsModeClick(View view)
    {
        if(Utils.readSharedIntSetting(getApplicationContext(), getString(R.string.AMOUNT_OF_ALGORITHMS_WARS), 0) >= 2)
        {
            switch (view.getId())
            {
                case R.id.cardViewChart:
                {
                    startActivity(new Intent(this, ChartActivity.class));
                    break;
                }
                case R.id.cardViewData:
                {
                    startActivity(new Intent(this, StatisticsActivity.class));
                    break;
                }
            }
        }
        else Toast.makeText(getApplicationContext(), getString(R.string.statisticConstraints), Toast.LENGTH_LONG).show();
    }
}
