package katarzyna.shakier;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;

import katarzyna.shakier.Database.AppDatabase;
import katarzyna.shakier.Database.DAOs.SortingResultsDao;
import katarzyna.shakier.Database.Models.SortingResults;
import katarzyna.shakier.Helpers.AlgorithmWarsResults;
import katarzyna.shakier.Helpers.CustomSortingResultsCardView;
import katarzyna.shakier.Helpers.Utils;

public class AlgorithmWarsSummary extends AppCompatActivity
{
    SortingResults[] sortingResults;
    SortingResultsDao sortingResultsDao;
    Bundle bundle;
    LinearLayout linearLayout;

    //winner and looser
    int winnerId=0;
    int looserId=0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_algorithm_wars_summary);

        linearLayout = findViewById(R.id.layout);
        sortingResultsDao = AppDatabase.getInstance(getApplicationContext()).sortingResultsDao();

        bundle = getIntent().getExtras();
        fillSortResultsTable();
        saveResultsToDatabse();
        chooseWinnerAndLooser();
        arrangeSortingResultsToCardView();
        Utils.incrementSharedIntSetting(getApplicationContext(), getString(R.string.AMOUNT_OF_ALGORITHMS_WARS));
    }

    private void fillSortResultsTable()
    {
        ArrayList<String> algorithmsSet = bundle.getStringArrayList(getString(R.string.algorithmsSet));
        int sortingSetSize = bundle.getInt(getString(R.string.listSize));
        long[] results = bundle.getLongArray(getString(R.string.resultsList));
        sortingResults = new SortingResults[results.length];

        for(int i=0; i<algorithmsSet.size(); i++)
        {
            sortingResults[i] = new SortingResults(algorithmsSet.get(i), sortingSetSize, results[i]);
        }
    }

    private void saveResultsToDatabse()
    {
        sortingResultsDao.insertAll(sortingResults);
    }

    private void chooseWinnerAndLooser()
    {
        long min=sortingResults[0].getSortingDurationTime();
        long max=sortingResults[0].getSortingDurationTime();

        for(int i=1; i<sortingResults.length; i++)
        {
            if(sortingResults[i].getSortingDurationTime() < min)
            {
                winnerId = i;
                min = sortingResults[i].getSortingDurationTime();
            }
            if(sortingResults[i].getSortingDurationTime() > max)
            {
                looserId = i;
                max = sortingResults[i].getSortingDurationTime();
            }
        }
    }

    private AlgorithmWarsResults returnAlgorithmStateById(int id)
    {
        if(winnerId == id)
        {
            return AlgorithmWarsResults.WINNER;
        }
        else
        {
            if(looserId == id)
            {
                return AlgorithmWarsResults.LOOSER;
            }
            else return AlgorithmWarsResults.NORMAL;
        }
    }

    private void arrangeSortingResultsToCardView()
    {
        for(int i=0; i< sortingResults.length; i++)
        {
            CustomSortingResultsCardView cardView = new CustomSortingResultsCardView(this);
            int icoId = getApplicationContext().getResources().getIdentifier(Utils.returnIcoAssetNameByAlgorithmName(sortingResults[i].getAlgorithmName()),
                    "drawable", getApplicationContext().getPackageName());

            cardView.Builder(
                    sortingResults[i].getAlgorithmName(),
                    sortingResults[i].getSortingDurationTime(),
                    getDrawable(icoId),
                    returnAlgorithmStateById(i)
            );
            linearLayout.addView(cardView);
            linearLayout.refreshDrawableState();
        }
    }

    public void goToWinnerActivity(View view)
    {
        startActivity(new Intent(AlgorithmWarsSummary.this, WinnerAlgorithmActivity.class)
        .putExtra(getString(R.string.algorithmName), sortingResults[winnerId].getAlgorithmName())
        .putExtra(getString(R.string.winnerTime), sortingResults[winnerId].getSortingDurationTime()));
    }
}
