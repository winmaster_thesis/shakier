package katarzyna.shakier.Helpers;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import katarzyna.shakier.R;

public class CustomChartLegend extends RelativeLayout
{
    Context context;
    View rootView;
    TextView textViewAlgName;
    ImageView imageViewColor;

    public CustomChartLegend(Context context)
    {
        super(context);
        this.context = context;
        Init(context);

    }

    public CustomChartLegend(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        this.context = context;
        Init(context);
    }

    private void Init(Context context)
    {
        rootView = inflate(context, R.layout.chart_legend, this);

        textViewAlgName = rootView.findViewById(R.id.textViewAlgName);
        imageViewColor = rootView.findViewById(R.id.imageViewColor);
    }

    public void Builder(String algName)
    {
        textViewAlgName.setText(algName);
        imageViewColor.setColorFilter(getResources().getColor(Utils.returnColorForChartByAlgorithmName(algName)));
    }
}
