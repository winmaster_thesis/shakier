package katarzyna.sortPackage.Sorting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import katarzyna.sortPackage.Helpers.CompareMethod;
import katarzyna.sortPackage.Helpers.SortingAlgorithms;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.model.SubcolumnValue;
import lecho.lib.hellocharts.util.ChartUtils;

public class BogoSort<T extends Comparable<T>> extends BaseSort
{
    private int shuffleCounter;

    public BogoSort(ArrayList<T> _sortArray, CompareMethod _compareMethod)
    {
        super(_sortArray, _compareMethod);
        shuffleCounter=0;
        algorithmName = SortingAlgorithms.BOGO_SORT;
    }

    public boolean isSorted()
    {
        boolean isOver=true;

        for(int i=0; i<sortArray.size()-1; i++)
        {
            if(sortComparator.compare(sortArray.get(i),sortArray.get(i+1))>0)
            {
                isOver=false;
                break;
            }
        }
        return isOver;
    }


    /*
        Every sortStep:
        - shufle the sortArray
     */

    public void sortStep()
    {
        Collections.shuffle(sortArray);
        shuffleCounter++;
    }

    public String printSortArray()
    {
        return  sortArray.toString();
    }

    public ColumnChartData produceChartData()
    {
        if(sortArray.get(0) instanceof Integer)
        {
            List<Column> columns = new ArrayList<>();
            for (int i = 0; i < sortArray.size(); i++)
            {
                List<SubcolumnValue> value = new ArrayList<>();
                value.add(new SubcolumnValue(Float.parseFloat(sortArray.get(i).toString()), ChartUtils.pickColor()));
                Column column = new Column(value);
                column.setHasLabels(true);
                columns.add(column);
            }
            return new ColumnChartData(columns);
        }
        return null;
    }
}
