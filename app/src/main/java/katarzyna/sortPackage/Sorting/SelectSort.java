package katarzyna.sortPackage.Sorting;

import java.util.ArrayList;
import java.util.List;

import katarzyna.sortPackage.Helpers.CompareMethod;
import katarzyna.sortPackage.Helpers.SortingAlgorithms;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.model.SubcolumnValue;
import lecho.lib.hellocharts.util.ChartUtils;

public class SelectSort<T extends Comparable<T>> extends BaseSort
{
    private ArrayList<T> destinationArray;

    public SelectSort(ArrayList<T> _sortArray, CompareMethod _compareMethod)
    {
        super(_sortArray, _compareMethod);
        algorithmName = SortingAlgorithms.SELECT_SORT;
        destinationArray=new ArrayList<>();
    }

    public boolean isSorted()
    {
        return sortArray.isEmpty();
    }

    /*
        Every sortStep:
        -find maximal or minimal value in set (depends on  compareMethod)
        -add this element to destinationArray
        -put first element from sortArray on min/max index
        -delete first element from sortArray
     */
    public void sortStep()
    {
        Object elem=sortArray.get(0);
        int elemIndex=0;

        for(Object t: sortArray)
        {
            if(sortComparator.compare(t,elem)<=0)
            {
                elem=t;
                elemIndex=sortArray.indexOf(t);
            }
        }

        destinationArray.add((T) elem);
        sortArray.set(elemIndex,sortArray.get(0));
        sortArray.remove(0);
    }

    public String printSortArray()
    {
        return  destinationArray+ "|"+sortArray;
    }

    @Override
    public ColumnChartData produceChartData()
    {
        if((!sortArray.isEmpty() && sortArray.get(0) instanceof Integer) || (!destinationArray.isEmpty() && destinationArray.get(0) instanceof Integer))
        {
            List<Column> columns = new ArrayList<>();
            for (int i = 0; i < destinationArray.size(); i++)
            {
                List<SubcolumnValue> value = new ArrayList<>();
                value.add(new SubcolumnValue(Float.parseFloat(destinationArray.get(i).toString()), ChartUtils.COLOR_GREEN));
                Column column = new Column(value);
                column.setHasLabels(true);
                columns.add(column);
            }
            for (int i = 0; i < sortArray.size(); i++)
            {
                List<SubcolumnValue> value = new ArrayList<>();
                value.add(new SubcolumnValue(Float.parseFloat(sortArray.get(i).toString()), ChartUtils.COLOR_BLUE));
                Column column = new Column(value);
                column.setHasLabels(true);
                columns.add(column);
            }
            return new ColumnChartData(columns);
        }
        return null;
    }
}