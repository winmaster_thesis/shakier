package katarzyna.shakier.AppFlow;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import katarzyna.shakier.Helpers.Utils;
import katarzyna.shakier.MainActivity;
import katarzyna.shakier.R;

public class SplashScreen extends AppCompatActivity
{
    private ImageView imageViewLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        Utils.incrementSharedIntSetting(getApplicationContext(), getString(R.string.AMOUNT_OF_APP_STARTS));

        imageViewLogo = findViewById(R.id.imageViewLogo);
        Animation splashAnimation = AnimationUtils.loadAnimation(this, R.anim.splash_anim);

        imageViewLogo.startAnimation(splashAnimation);

        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                Pair<View, String> logo = Pair.create((View)imageViewLogo, "sharedLogoTransition");

                ActivityOptions transitionActivityOptions = ActivityOptions.makeSceneTransitionAnimation(SplashScreen.this, logo);
                SplashScreen.this.startActivity(new Intent(SplashScreen.this, MainActivity.class), transitionActivityOptions.toBundle());
            }
        }, getResources().getInteger(R.integer.splashScreenLength));
    }
}
