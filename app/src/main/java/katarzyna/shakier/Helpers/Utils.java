package katarzyna.shakier.Helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import katarzyna.shakier.Database.Models.Words;
import katarzyna.shakier.R;

public class Utils
{
    public static String[] algorithms = {
            "Bubble Sort",
            "Insert Sort",
            "Select Sort",
            "Bogo Sort",
            "Gnome Sort"};

    public static String readSharedStringSetting(Context ctx, String settingName, String defaultValue)
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(ctx);
        return sharedPref.getString(settingName, defaultValue);
    }

    public static boolean readSharedBoolSetting(Context ctx, String settingName, boolean defaultValue)
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(ctx);
        return sharedPref.getBoolean(settingName, defaultValue);
    }

    public static int readSharedIntSetting(Context ctx, String settingName, int defaultValue)
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(ctx);
        return sharedPref.getInt(settingName, defaultValue);
    }

    public static void saveSharedStringSetting(Context ctx, String settingName, String settingValue)
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(ctx);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(settingName, settingValue);
        editor.apply();
    }

    public static void saveSharedBoolSetting(Context ctx, String settingName, boolean settingValue)
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(ctx);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(settingName, settingValue);
        editor.apply();
    }

    public static void saveSharedIntSetting(Context ctx, String settingName, int settingValue)
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(ctx);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(settingName, settingValue);
        editor.apply();
    }

    public static void incrementSharedIntSetting(Context ctx, String settingName)
    {
        int value = readSharedIntSetting(ctx, settingName, 0);
        saveSharedIntSetting(ctx, settingName, value+1);
    }

    public static void resetSharedSettingsToDefault(Context ctx)
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(ctx);
        sharedPref.edit().clear().apply();
    }

    public static ArrayList<String> convertWordsToStringArray(ArrayList<Words> words)
    {
        ArrayList<String> result = new ArrayList<>();
        for(Words word: words)
        {
            result.add(word.getWord());
        }
        return result;
    }

    public static String returnIcoAssetNameByAlgorithmName(String algorithmName)
    {
       switch (algorithmName)
       {
           case "Bubble Sort":
           {
                return "bubble";
           }
           case "Insert Sort":
           {
               return "insert";
           }
           case "Select Sort":
           {
               return "select";
           }
           case "Bogo Sort":
           {
               return "bogo";
           }
           case "Gnome Sort":
           {
               return "gnome";
           }
           default:
           {
               return "shakier_logo";
           }
       }
    }

    public static String returnSharedPrefNameByAlgorithmName(String algorithmName)
    {
        switch (algorithmName)
        {
            case "Bubble Sort":
            {
                return "BUBBLE_SORT_RUNS";
            }
            case "Insert Sort":
            {
                return "INSERT_SORT_RUNS";
            }
            case "Select Sort":
            {
                return "SELECT_SORT_RUNS";
            }
            case "Bogo Sort":
            {
                return "BOGO_SORT_RUNS";
            }
            case "Gnome Sort":
            {
                return "GNOME_SORT_RUNS";
            }
        }

        return null;
    }

    public static int returnColorForChartByAlgorithmName(String algorithmName)
    {
        switch (algorithmName)
        {
            case "Bubble Sort":
            {
                return R.color.colorOrdinary;
            }
            case "Insert Sort":
            {
                return R.color.colorPrimary;
            }
            case "Select Sort":
            {
                return R.color.colorAccentRev;
            }
            case "Bogo Sort":
            {
                return R.color.colorAccent;
            }
            case "Gnome Sort":
            {
                return R.color.background_desert;
            }
            default:
            {
                return R.color.colorDisabled;
            }
        }
    }

    public static String getSortingDurationTime(long startTime, long endTime)
    {
        long millis = endTime - startTime;
        int seconds = (int) (millis / 1000);
        int minutes = seconds / 60;
        seconds     = seconds % 60;

        return  String.format("%d:%02d", minutes, seconds);
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public static boolean completeAllStepByStepAchievement(Context context)
    {
        for(String algorithm : algorithms)
        {
            if(readSharedIntSetting(context, returnSharedPrefNameByAlgorithmName(algorithm),0) <1)
            {
                return false;
            }
        }
        return true;
    }
}
